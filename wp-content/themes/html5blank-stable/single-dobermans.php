<?php get_header(); ?>

<div class="components" id="components">
			<?php
			if( have_rows('components') ):
				while ( have_rows('components') ) : the_row();
					if( get_row_layout() == 'component_1' ):
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$button_text = get_sub_field('button_text');
						$background = get_sub_field('background');?>
								
							<section class="component_1">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="col-12" style="padding-left:0; padding-right:0;">
												<div class="component_1-image" style="background-image: url(<?php if( $background ): ?>
    												<?php echo $background; ?>
													<?php endif; ?>)">
												</div>
											
												<div class="component_1-box">
													<div class="component_1-title"><?php echo $title ?></div>
													<div class="component_1-text"><?php echo $description ?></div>
													<div class="component_1-button"><?php echo $button_text ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
										
					<?php
					elseif( get_row_layout() == 'component_2' ):
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$button_text = get_sub_field('button_text');
						$background = get_sub_field('img_right');?>
				
							<section class="component_2">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="col-lg-5 col-sm-12 col-md-12 comp2-left-area order-last order-md-first">
												<div class="component_2-box">
													<div class="component_2-title"><?php echo $title ?></div>
													<div class="component_2-text"><?php echo $description ?></div>
													<div class="component_2-button"><?php echo $button_text ?></div>
												</div>
											</div>

											<div class="col-lg-7 col-sm-12 col-md-12 comp2-right-area order-sm-first">
												<?php if( !empty( $background ) ): ?>
													<div class="component_2-image">
														<img src="<?php echo esc_url($background['url']); ?>" alt="<?php echo esc_attr($background['alt']); ?>" />
													</div>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</section>


					<?php

					elseif( get_row_layout() == 'section-title' ):
						$title = get_sub_field('title');
						?>

							<section class="section-title">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="puppies-title-box">
												<h1 class="col-12 puppies-title"><?php echo $title?></h1>
											</div>
										</div>
									</div>
								</div>
							</section>

					<?php
					elseif( get_row_layout() == 'video_banner' ):
						$video_file = get_sub_field('video_file');
						$title = get_sub_field('title');
						$description = get_sub_field('description');?>
				
							<section id="banner">
								<div class="container-fluid">
									<div class="container">

										<video autoplay muted loop playsinline id="doberman-video">
											<source src=<?php echo $video_file?> type="video/mp4">
										</video>
											
										<div class="row">
											<div class="col-lg-6 col-sm-12 banner-box">
												<h2 class="title"><?php echo $title ?></h2>
												<p class="desription"><?php echo $description ?></p>
												<span class="banner">POZNAJ JAK DZIAŁAMY</span>
											</div>
										</div>
									</div>
								</div>
							</section>

					<?php
					elseif( get_row_layout() == 'template' ):
						$template = get_sub_field('template');?>
				
							<?php include( locate_template( $template , false, false )); ?>

							<?php
					elseif( get_row_layout() == 'img-banner' ):
					$title = get_sub_field('title');
					$background = get_sub_field('background');
					$mobile_background = get_sub_field('mobile_background');
					?>

						<section class="img-banner">
							<div class="container-fluid">
								<div class="container">
									<div class="row desktop">
										<?php if( !empty( $background ) ): ?>
											<div class="col-12 img-banner-background">
											<img src="<?php echo get_template_directory_uri(); ?>/img/pix.png" width="1920px" height="715px" alt="" class="pix">
											<img 
												class='lazy' 
												data-src="<?php echo $background['sizes']['large']; ?>" 
												alt="<?php echo $background['title'];?>" 
												width="<?php echo $background['width'];?>" 
												height="<?php echo $background['height'];?>" 
												data-srcset="
												<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
												<?php echo $background['url'].' '.$background['width'].'w'; ?>,
												<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
												<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >


												
												<h3 class="title"><?php echo $title ?></h3>
												
											</div>
											
										<?php endif; ?>
									</div>

									<div class="row mobile">
										<?php if( !empty( $mobile_background ) ): ?>
											<div class="col-12 img-banner-background">
											<img src="<?php echo get_template_directory_uri(); ?>/img/pix.png" width="1920px" height="715px" alt="" class="pix">
											<img 
												class='lazy' 
												data-src="<?php echo $mobile_background['sizes']['large']; ?>" 
												alt="<?php echo $mobile_background['title'];?>" 
												width="<?php echo $mobile_background['width'];?>" 
												height="<?php echo $mobile_background['height'];?>" 
												data-srcset="
												<?php echo $mobile_background['sizes']['large'].' '.$mobile_background['sizes']['large-width'].'w'; ?>, 
												<?php echo $mobile_background['url'].' '.$mobile_background['width'].'w'; ?>,
												<?php echo $mobile_background['sizes']['medium'].' '.$mobile_background['sizes']['medium-width'].'w';?>, 
												<?php echo $mobile_background['sizes']['small'].' '.$mobile_background['sizes']['small-width'].'w'; ?>" >


												
												<h3 class="title"><?php echo $title ?></h3>
												
											</div>
											
										<?php endif; ?>
									</div>

									<div class="row arrow">
										<div class="col-lg-6 col-sm-12 link-box">	
											<a href="<?php echo get_home_url ();?>#scrolldown" class="ca3-scroll-down-link ca3-scroll-down-arrow" data-ca3_iconfont="ETmodules" data-ca3_icon=""></a>
										</div>
									</div>
										<hr id="scrolldown">
								</div>
							</div>
						</section>		

						<?php
					elseif( get_row_layout() == 'component_3a' ):
						$img = get_sub_field('img_left');
						$title = get_sub_field('title');
						$description = get_sub_field('description');?>

						<section class="component_3a wow animate__fadeInUp" data-wow-duration="2s">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										
									
										<?php if( !empty( $img ) ): ?>
											<div class="col-lg-4 col-md-12 img_left">

											<img 
												class='lazy' 
												data-src="<?php echo $img['sizes']['large']; ?>" 
												alt="<?php echo $img['title'];?>" 
												width="<?php echo $img['width'];?>" 
												height="<?php echo $img['height'];?>" 
												data-srcset="
												<?php echo $img['sizes']['large'].' '.$img['sizes']['large-width'].'w'; ?>, 
												<?php echo $img['url'].' '.$img['width'].'w'; ?>,
												<?php echo $img['sizes']['medium'].' '.$img['sizes']['medium-width'].'w';?>, 
												<?php echo $img['sizes']['small'].' '.$img['sizes']['small-width'].'w'; ?>" >
											</div>
										<?php endif; ?>
										<div class="col-lg-8 col-md-12 text_area">
											<div class="textbox wow animate__fadeIn" data-wow-duration="1s">
												<h3 class="title"><?php echo $title ?></h3>
												<p class="desription"><?php echo $description ?></p>
											</div>
										</div>
									</div>


									</div>
								</div>
							</div>
						</section>		

						<?php
					elseif( get_row_layout() == 'gallery' ):
						$images = get_sub_field('gallery');
						$title = get_sub_field('title');
						?>

						<section class="gallery">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="title"><?php echo $title ?></div>
									</div>

                                    <div class="row">
										
                                  
									
                                    <?php 
                                        if( $images ): ?>
                                            
                                                
													<?php echo esc_url($image['url']); ?>
													<div id="gallery" style="display:none;">
													<?php foreach( $images as $image ): ?>
                                                            <img data-src="<?php echo esc_url($image['sizes']['large']); ?>" data-image="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="lazy"/>
															<?php endforeach; ?>
													</div>
                                                    
                                                
												
                                        <?php endif; ?>

                                        </div>
									
								</div>
							</div>
						</section>


						


						<?php
					elseif( get_row_layout() == 'video_gallery' ):
						$title = get_sub_field('title');
						?>

						<section class="video_gallery" >
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="title"><?php echo $title ?></div>
									</div>
                                    <div class="row">

									<?php 
									$video_gallery = get_sub_field('video_gallery');
									$count = count($video_gallery);
									$first_frame = '#t=0.5';
									

									foreach ($video_gallery as $video_file) {
										
										if ($count==1) {
											?>
											<div class="col-lg-12 col-sm-12 video_file_box">
											<video muted controls id="doberman-video">
											<source src=<?php echo implode($video_file) ?> type="video/mp4">
											</video></div><?php
										}elseif ($count==2) {
											?>
											<div class="col-lg-6 col-sm-12 video_file_box">
											<video muted controls id="doberman-video">
											<source src=<?php echo implode($video_file) ?> type="video/mp4">
											</video></div><?php
										}elseif ($count==3) {
											?>
											<div class="col-lg-4 col-sm-12 video_file_box">
											<video muted controls id="doberman-video" preload="metadata">
											<source src=<?php echo implode($video_file) .$first_frame ?> type="video/mp4">
											</video></div><?php
										}elseif ($count==4) {
											?>
											<div class="col-lg-3 col-sm-12 video_file_box">
											<video muted controls id="doberman-video" preload="metadata">
											<source src=<?php echo implode($video_file) .$first_frame ?> type="video/mp4">
											</video></div><?php
										}
									}
									
 									?>
					
								</div>
							</div>
						</section>

					<?php
					endif;
								
				endwhile;
				else :
				endif;
			?>
			</div>
	<?php wp_reset_postdata(); ?>

				<?php
			// Check value exists.
			if( have_rows('flexible_content') ):

				// Loop through rows.
				while ( have_rows('flexible_content') ) : the_row();
				
							if( get_row_layout() == 'content' ):

								if( have_rows('section') ):
									while ( have_rows('section') ) : the_row();
									if( get_row_layout() == 'columns_1' ):
										$col_1 = get_sub_field('col_1');
										echo '<div class="row">';
										echo '<div class="col-12">'.$col_1.'</div>';
										echo '</div>';

									elseif( get_row_layout() == 'columns_2' ):
										$col_1 = get_sub_field('col_1');
										$col_2 = get_sub_field('col_2');

										echo '<div class="row">';
											echo '<div class="col-lg-6 col-sm-12 col-md-12">'.$col_1.'</div>';
											echo '<div class="col-lg-6 col-sm-12 col-md-12">'.$col_2.'</div>';
										echo '</div>';
									elseif( get_row_layout() == 'columns_3' ):
										$col_1 = get_sub_field('col_1');
										$col_2 = get_sub_field('col_2');
										$col_3 = get_sub_field('col_3');

										echo '<div class="row">';
										echo '<div class="col-4">'.$col_1.'</div>';
										echo '<div class="col-4">'.$col_2.'</div>';
										echo '<div class="col-4">'.$col_3.'</div>';
									echo '</div>';
									endif;	
								
									endwhile;
								endif;
							endif;
						
							
				// End loop.
				endwhile;

			// No value.
			else :
				// Do something...
			endif;

?>

<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
