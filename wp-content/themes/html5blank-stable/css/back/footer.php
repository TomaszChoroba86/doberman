			<!-- footer -->
			<footer class="container-fluid" id="footer" role="contentinfo">
				<div class="container">

				<div class="row">
					<div class="col-lg-4 col-md-12 footer-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/doggo_logo.png">
					</div>
					<div class="col-lg-1 col-md-12">
						<ul>
							<li><h3>Informacje</h3></li>
							<li><p><a href="<?php echo get_home_url ();?>/o-nas/">O nas</a></p></li>
							<li><p><a href="<?php echo get_home_url ();?>/nasza-oferta/">Nasza Oferta</a></p></li>
							<li><p class="link-box"><a href="<?php echo get_home_url ();?>/#psy">Psy</a></p></li>
						</ul>
					</div>
					<div class="col-lg-1 col-md-12">
						<ul>
							<li><h3>Inne</h3></li>
							<li><p class="link-box"><a href="<?php echo get_home_url ();?>/#hotel">Hotel</a></p></li>
							<li><p class="link-box"><a href="<?php echo get_home_url ();?>/#shop">Sklep</a></p></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-12">
						<h3>Kontakt</h3>
						<div class="contact-item">
							<span>
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/at.png" class="lazy">
								<a class="footer-link" href="mailto:<?php echo get_field('footer_email');?>"><?php echo get_field('footer_email');?></a>
							</span>
						</div>
						<div class="contact-item">	
							<span>
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/call.png" class="lazy">
								<a><?php echo get_field('footer_phone', 51);?></a>
							</span>
						</div>	
					</div>
					<div class="col-lg-2 col-md-12 social-ico">
						<a href="https://www.facebook.com/Dobermanproject.eu"><img data-src="<?php echo get_template_directory_uri(); ?>/img/fb-ico.png" class="lazy fb-ico"></a>
						<a href="https://www.instagram.com/dobermanproject.eu/"><img data-src="<?php echo get_template_directory_uri(); ?>/img/ig-ico.png" class="lazy ig-ico"></a>
					</div>
					<div class="col-lg-1 col-md-12">
					</div>
				</div>
				<div class="row footer-bottom">
					<div class="col-lg-6 col-md-12">
						<p class="policy"><a href="<?php echo get_home_url ();?>/polityka-prywatnosci/">Polityka prywatności</a></p>
					</div>
					<div class="col-lg-6 col-md-12">
						<p class="rights">Wszystkie prawa zastrzeżone</p>
					</div>
				</div>
				</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->


	</body>
</html>
