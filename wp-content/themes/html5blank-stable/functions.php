<?php

function wpb_custom_new_menu() {
    register_nav_menus(
      array(
        'doberman-menu' => __('Doberman Menu'),
        // 'informacje' => __('Informacje')
      )
    );
  }
  add_action( 'init', 'wpb_custom_new_menu' );
  add_theme_support( 'post-thumbnails' );
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// $wpml_global_id = apply_filters ('wpml_object_id', $post_id, 'post' );
// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    // if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	// wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        // wp_enqueue_script('conditionizr'); // Enqueue it!

        // wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        // wp_enqueue_script('modernizr'); // Enqueue it!

        // wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        // wp_enqueue_script('html5blankscripts'); // Enqueue it!
    // }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_register_script('jquerylazy', get_template_directory_uri() . '/js/basic_jquery_3.5.1_lazy_load_cookies.js', array()); // Conditional script(s)
        wp_enqueue_script('jquerylazy'); // Enqueue it!
        $postID = get_the_ID();
        $wpml_global_id = apply_filters ('wpml_object_id', $postID, 'post', true, 'pl' );
        $postID = $wpml_global_id;
        // var_dump($postID);
        $posttype = get_post_type();

        wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js', array()); // Conditional script(s)
        wp_enqueue_script('scripts'); // Enqueue it!

       
        switch ( $postID ) :
            case 51 :
                //Homepage 
                
                wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
                wp_enqueue_script('simpleParallax'); // Enqueue it!

                wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
                wp_enqueue_script('owl-carousel'); // Enqueue it!

                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!

                wp_register_script('puppies', get_template_directory_uri() . '/js/puppies.js', array()); // Conditional script(s)
                wp_enqueue_script('puppies'); // Enqueue it!

                wp_register_script('home', get_template_directory_uri() . '/js/home.js', array()); // Conditional script(s)
                wp_enqueue_script('home'); // Enqueue it!

            break;

        endswitch;
        switch ( $postID ) :
            case 291 :
                //O Nas 
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!
                
            break;

        endswitch;
        switch ( $postID ) :
            case 247 :
                //Nasza Oferta 
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!
                
            break;

        endswitch;
        switch ( $postID ) :
            case 318 :
                //O rasie 
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!

                wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
                wp_enqueue_script('simpleParallax'); // Enqueue it!

                wp_register_script('o-rasie', get_template_directory_uri() . '/js/o-rasie.js', array()); // Conditional script(s)
                wp_enqueue_script('o-rasie'); // Enqueue it!
                
            break;

        endswitch;
        switch ( $postID ) :
            case 175 :
                //Szczenięta 
                wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
                wp_enqueue_script('owl-carousel'); // Enqueue it!
                
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!

                wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
                wp_enqueue_script('simpleParallax'); // Enqueue it!

                wp_register_script('puppies', get_template_directory_uri() . '/js/puppies.js', array()); // Conditional script(s)
                wp_enqueue_script('puppies'); // Enqueue it!

                wp_register_script('szczenieta', get_template_directory_uri() . '/js/szczenieta.js', array()); // Conditional script(s)
                wp_enqueue_script('szczenieta'); // Enqueue it!

            break;

        endswitch;
        switch ( $postID ) :
            case 361 :
                //Nasze dobermany 
                wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
                wp_enqueue_script('owl-carousel'); // Enqueue it!

                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!

                wp_register_script('dobermans', get_template_directory_uri() . '/js/dobermans.js', array()); // Conditional script(s)
                wp_enqueue_script('dobermans'); // Enqueue it!

            break;

        endswitch;
        switch ( $postID ) :
            case 3 :
                //Polityka prywatności 
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!
            
            break;

            endswitch;
            switch ( $postID ) :
                case 401 :
                    // EN-DE 
                    wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                    wp_enqueue_script('wow-js'); // Enqueue it!
                        
                
                break;

        endswitch;
        switch ( $postID ) :
            case 1143 :
                // Transport 
                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!
                    
            
            break;

    endswitch;
        switch ( $posttype ) :
                case 'puppies' :
                // Szczenięta - custom posts 

                wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
                wp_enqueue_script('owl-carousel'); // Enqueue it!

                wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
                wp_enqueue_script('wow-js'); // Enqueue it!

                wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
                wp_enqueue_script('simpleParallax'); // Enqueue it!

                wp_register_script('puppies', get_template_directory_uri() . '/js/puppies.js', array()); // Conditional script(s)
                wp_enqueue_script('puppies'); // Enqueue it!
                
                wp_register_script('unitegallery', get_template_directory_uri() . '/js/unitegallery.js', array()); // Conditional script(s)
                wp_enqueue_script('unitegallery'); // Enqueue it!

                wp_register_script('ug-theme-tiles', get_template_directory_uri() . '/js/ug-theme-tiles.js', array()); // Conditional script(s)
                wp_enqueue_script('ug-theme-tiles'); // Enqueue it!

                wp_register_script('szczenie', get_template_directory_uri() . '/js/szczenie.js', array()); // Conditional script(s)
                wp_enqueue_script('szczenie'); // Enqueue it!s

            
                break;

        endswitch;

        switch ( $posttype ) :
            case 'szczenieta' :
            // Szczenięta - custom posts 

            wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
            wp_enqueue_script('owl-carousel'); // Enqueue it!

            wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
            wp_enqueue_script('wow-js'); // Enqueue it!

            wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
            wp_enqueue_script('simpleParallax'); // Enqueue it!

            wp_register_script('puppies', get_template_directory_uri() . '/js/puppies.js', array()); // Conditional script(s)
            wp_enqueue_script('puppies'); // Enqueue it!
            
            wp_register_script('unitegallery', get_template_directory_uri() . '/js/unitegallery.js', array()); // Conditional script(s)
            wp_enqueue_script('unitegallery'); // Enqueue it!

            wp_register_script('ug-theme-tiles', get_template_directory_uri() . '/js/ug-theme-tiles.js', array()); // Conditional script(s)
            wp_enqueue_script('ug-theme-tiles'); // Enqueue it!

            wp_register_script('szczenie', get_template_directory_uri() . '/js/szczenie.js', array()); // Conditional script(s)
            wp_enqueue_script('szczenie'); // Enqueue it!s

        
            break;

    endswitch;


        switch ( $posttype ) :
            case 'dobermans' :
            // Dobermans - custom posts 

            wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array()); // Conditional script(s)
            wp_enqueue_script('owl-carousel'); // Enqueue it!

            wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array()); // Conditional script(s)
            wp_enqueue_script('wow-js'); // Enqueue it!

            wp_register_script('simpleParallax', get_template_directory_uri() . '/js/simpleParallax.js', array()); // Conditional script(s)
            wp_enqueue_script('simpleParallax'); // Enqueue it!

            wp_register_script('puppies', get_template_directory_uri() . '/js/puppies.js', array()); // Conditional script(s)
            wp_enqueue_script('puppies'); // Enqueue it!
        
            wp_register_script('unitegallery', get_template_directory_uri() . '/js/unitegallery.js', array()); // Conditional script(s)
            wp_enqueue_script('unitegallery'); // Enqueue it!

            wp_register_script('ug-theme-tiles', get_template_directory_uri() . '/js/ug-theme-tiles.js', array()); // Conditional script(s)
            wp_enqueue_script('ug-theme-tiles'); // Enqueue it!

            wp_register_script('doberman', get_template_directory_uri() . '/js/doberman.js', array()); // Conditional script(s)
            wp_enqueue_script('doberman'); // Enqueue it!
            break;

    endswitch;

    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    $postID = get_the_ID();
    $posttype = get_post_type();
    $postparent = wp_get_post_parent_id($postID);
  
    wp_register_style('loading', get_template_directory_uri() . '/css/loading.css',array(), '3.0', 'all');
    wp_enqueue_style('loading');

    wp_register_style('normalize', get_template_directory_uri() . '/css/basic.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!


switch ( $posttype ) :
        case 'puppies' :
        // Szczenięta - custom posts 

    wp_register_style('unitegallery', get_template_directory_uri() . '/css/unite-gallery.css', array(), '1.0', 'all');
    wp_enqueue_style('unitegallery'); // Enqueue it!

    wp_register_style('ug-theme-default', get_template_directory_uri() . '/css/ug-theme-default.css', array(), '1.0', 'all');
    wp_enqueue_style('ug-theme-default'); // Enqueue it!
    break;

endswitch;

switch ( $posttype ) :
    case 'szczenieta' :
    // Szczenięta - custom posts 

wp_register_style('unitegallery', get_template_directory_uri() . '/css/unite-gallery.css', array(), '1.0', 'all');
wp_enqueue_style('unitegallery'); // Enqueue it!

wp_register_style('ug-theme-default', get_template_directory_uri() . '/css/ug-theme-default.css', array(), '1.0', 'all');
wp_enqueue_style('ug-theme-default'); // Enqueue it!
break;

endswitch;

switch ( $posttype ) :
    case 'dobermans' :
    // Dobermans - custom posts  

wp_register_style('unitegallery', get_template_directory_uri() . '/css/unite-gallery.css', array(), '1.0', 'all');
wp_enqueue_style('unitegallery'); // Enqueue it!

wp_register_style('ug-theme-default', get_template_directory_uri() . '/css/ug-theme-default.css', array(), '1.0', 'all');
wp_enqueue_style('ug-theme-default'); // Enqueue it!
break;

endswitch;
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank'), // Extra Navigation if needed (duplicate as many as you need!)
        'informacje-menu' => __('Informacje Menu', 'html5blank'), 
        'inne-menu' => __('Inne Menu', 'html5blank')
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

function add_nbsp($title, $id = null){
    $title = preg_replace('/ ([A-Za-z+-D]{1}) /', " $1&nbsp;", $title);
    return preg_replace('/ ([A-Za-z+-D]{1}&nbsp;[A-Za-z+-D]{1}) /', " $1&nbsp;", $title);
  }

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
// add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
// add_action('init', 'create_post_type_puppies'); // Add Nasze Szczenieta 
add_action('init', 'create_post_type_dobermans'); // Add Nasze Dobermany
add_action('init', 'create_post_type_szczenieta'); // Add Nasze Szczenieta 
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
// function create_post_type_html5()
// {
//     register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
//     register_taxonomy_for_object_type('post_tag', 'html5-blank');
//     register_post_type('html5-blank', // Register Custom Post Type
//         array(
//         'labels' => array(
//             'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
//             'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
//             'add_new' => __('Add New', 'html5blank'),
//             'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
//             'edit' => __('Edit', 'html5blank'),
//             'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
//             'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
//             'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
//             'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
//             'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
//             'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
//             'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
//         ),
//         'public' => true,
//         'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
//         'has_archive' => true,
//         'supports' => array(
//             'title',
//             'editor',
//             'excerpt',
//             'thumbnail'
//         ), // Go to Dashboard Custom HTML5 Blank post for supports
//         'can_export' => true, // Allows export in Tools > Export
//         'taxonomies' => array(
//             'post_tag',
//             'category'
//         ) // Add Category and Post Tags support
//     ));
// }
function create_post_type_szczenieta()
{
    register_taxonomy_for_object_type('category', 'szczenieta-post'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'szczenieta-post');
    register_post_type('szczenieta', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Nasze szczenięta', 'szczenieta-post'), // Rename these to suit
            'singular_name' => __('Nasze szczenię', 'szczenieta-post'),
            'add_new' => __('Add New', 'szczenieta-post'),
            'add_new_item' => __('Add New Nasze szczenięta', 'szczenieta-post'),
            'edit' => __('Edit', 'szczenieta-post'),
            'edit_item' => __('Edit Nasze szczenięta', 'szczenieta-post'),
            'new_item' => __('New Nasze szczenięta', 'szczenieta-post'),
            'view' => __('View Nasze szczenięta', 'szczenieta-post'),
            'view_item' => __('View Nasze szczenięta', 'szczenieta-post'),
            'search_items' => __('Search Nasze szczenięta', 'szczenieta-post'),
            'not_found' => __('No Nasze szczenięta Posts found', 'szczenieta-post'),
            'not_found_in_trash' => __('No Nasze szczenięta Posts found in Trash', 'szczenieta-post')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'menu_icon'   => 'dashicons-pets',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

//add custom column 
function add_acf_columns_szczenieta ( $columns ) {
    return array_merge ( $columns, array (
        'active' => __ ( 'Aktywny')
    ) );
}
add_filter ('manage_popup_posts_columns_szczenieta', 'add_acf_columns_szczenieta');

function popup_custom_column_szczenieta ( $column, $post_id ) {
    switch ( $column ) {
        case 'active':
            if(get_field('szczenieta_status', $post_id)==1) {
                $str = 'tak';
            } else {
                $str = 'nie';
            }
            echo $str;
        break;
    }
}
add_action ( 'manage_popup_posts_custom_column', 'popup_custom_column', 10, 2 );

function create_post_type_dobermans()
{
    register_taxonomy_for_object_type('category', 'dobermans-post'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'dobermans-post');
    register_post_type('dobermans', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Nasze dobermany', 'dobermans-post'), // Rename these to suit
            'singular_name' => __('Nasz doberman', 'dobermans-post'),
            'add_new' => __('Add New', 'dobermans-post'),
            'add_new_item' => __('Add New Nasze dobermany', 'dobermans-post'),
            'edit' => __('Edit', 'dobermans-post'),
            'edit_item' => __('Edit Nasze dobermany', 'dobermans-post'),
            'new_item' => __('New Nasze dobermany', 'pdobermans-post'),
            'view' => __('View Nasze dobermany', 'dobermans-post'),
            'view_item' => __('View Nasze dobermany', 'dobermans-post'),
            'search_items' => __('Search Nasze dobermany', 'dobermans-post'),
            'not_found' => __('No Nasze dobermany Posts found', 'dobermans-post'),
            'not_found_in_trash' => __('No Nasze dobermany Posts found in Trash', 'dobermans-post')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_icon'   => 'dashicons-awards',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

// //add custom column 
function add_acf_columns ( $columns ) {
    return array_merge ( $columns, array (
        'active' => __ ( 'Aktywny')
    ) );
}
add_filter ('manage_popup_posts_columns', 'add_acf_columns');

function popup_custom_column ( $column, $post_id ) {
    switch ( $column ) {
        case 'active':
            if(get_field('puppies_status', $post_id)==1) {
                $str = 'tak';
            } else {
                $str = 'nie';
            }
            echo $str;
        break;
    }
}
add_action ( 'manage_popup_posts_custom_column', 'popup_custom_column', 10, 2 );

//add custom column 
// function add_acf_columns_dobermans ( $columns ) {
//     return array_merge ( $columns, array (
//         'active' => __ ( 'Aktywny')
//     ) );
// }
// add_filter ('manage_popup_posts_columns_dobermans', 'add_acf_columns_dobermans');

// function popup_custom_column_dobermans ( $column, $post_id ) {
//     switch ( $column ) {
//         case 'active':
//             if(get_field('dobermans_status', $post_id)==1) {
//                 $str = 'tak';
//             } else {
//                 $str = 'nie';
//             }
//             echo $str;
//         break;
//     }
// }
// add_action ( 'manage_popup_posts_custom_column', 'popup_custom_column', 10, 2 );

// function create_post_type_puppies()
// {
//     register_taxonomy_for_object_type('category', 'puppies-post'); // Register Taxonomies for Category
//     register_taxonomy_for_object_type('post_tag', 'puppies-post');
//     register_post_type('puppies', // Register Custom Post Type
//         array(
//         'labels' => array(
//             'name' => __('Nasze szczenięta', 'puppies-post'), // Rename these to suit
//             'singular_name' => __('Nasze szczenię', 'puppies-post'),
//             'add_new' => __('Add New', 'puppies-post'),
//             'add_new_item' => __('Add New Nasze szczenięta', 'puppies-post'),
//             'edit' => __('Edit', 'puppies-post'),
//             'edit_item' => __('Edit Nasze szczenięta', 'puppies-post'),
//             'new_item' => __('New Nasze szczenięta', 'puppies-post'),
//             'view' => __('View Nasze szczenięta', 'puppies-post'),
//             'view_item' => __('View Nasze szczenięta', 'puppies-post'),
//             'search_items' => __('Search Nasze szczenięta', 'puppies-post'),
//             'not_found' => __('No Nasze szczenięta Posts found', 'puppies-post'),
//             'not_found_in_trash' => __('No Nasze szczenięta Posts found in Trash', 'puppies-post')
//         ),
//         'public' => true,
//         'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
//         'has_archive' => true,
//         'menu_icon'   => 'dashicons-pets',
//         'supports' => array(
//             'title',
//             'editor',
//             'excerpt',
//             'thumbnail'
//         ), // Go to Dashboard Custom HTML5 Blank post for supports
//         'can_export' => true, // Allows export in Tools > Export
//         'taxonomies' => array(
//             'post_tag',
//             'category'
//         ) // Add Category and Post Tags support
//     ));
// }


// //add custom column 
// function add_acf_columns ( $columns ) {
//     return array_merge ( $columns, array (
//         'active' => __ ( 'Aktywny')
//     ) );
// }
// add_filter ('manage_popup_posts_columns', 'add_acf_columns');

// function popup_custom_column ( $column, $post_id ) {
//     switch ( $column ) {
//         case 'active':
//             if(get_field('puppies_status', $post_id)==1) {
//                 $str = 'tak';
//             } else {
//                 $str = 'nie';
//             }
//             echo $str;
//         break;
//     }
// }
// add_action ( 'manage_popup_posts_custom_column', 'popup_custom_column', 10, 2 );
/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

// flush_rewrite_rules()

function stackoverflow_30817906( $content, $post_id, $thumbnail_id ) {
    if ( 'page' == get_post_type( $page_id ) ) {
        return $content;
    }
    $caption = '<p>' . esc_html__( 'Rekomendowany rozmiar obrazka: 1540px (wysokość) x 1080px (szerokość)', 'i18n-tag' ) . '</p>';
    return $content . $caption;
}
add_filter( 'admin_post_thumbnail_html', 'stackoverflow_30817906', 10, 3 );



// $all_scripts = [];
// global $all_scripts;
// add_filter('script_loader_tag', 'update_script_tag', 10, 3 );

// function is_login_page() {
//     return !strncmp($_SERVER['REQUEST_URI'], '/wp-login.php', strlen('/wp-login.php'));
// }

// function update_script_tag( $tag, $handle, $src ) {

//     if(is_admin()==false && is_login_page()==false){
//       if ( $handle == 'jquery-core' /*|| $handle == 'jquery-migrate'*/) {
//         // var_dump($handle);
//         $js_File = file_get_contents($src);
//         echo '<!-- '.$handle.' -->';
//         echo '<script>'.$js_File.'</script>';
//         echo '<!-- '.$handle.' ends -->';
//             // return $tag;
//         return;
//         }
//       array_push($GLOBALS['all_scripts'],[$handle,$src]);
//         return;
//     }else{
//       return $tag;
//     }
//   }

//   $all_styles = [];
// global $all_styles;

// add_filter('style_loader_tag', 'update_style_tag', 10, 4 );

// function update_style_tag( $tag, $handle, $src, $media ) {
//   if(is_admin()==false && is_login_page()==false){
//     if ( $handle === 'loading' ) {
//   		// return $tag;
//       echo '<!-- '.$handle.' -->';
//       $css_File = file_get_contents(get_template_directory_uri().'/css/loading.css');
//       // $css_File ="#preloader{width:100vw;height:100vh;position:fixed;background:#fff;top:0;left:0;opacity:1;z-index:99999}#preloader img{position:absolute;left:50%;top:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);-webkit-animation-name:opacity_loop;animation-name:opacity_loop;-webkit-animation-duration:2s;animation-duration:2s;opacity:0;-webkit-animation-iteration-count:4;animation-iteration-count:4}@media (max-width:400px){#preloader img{width:60%;height:auto}}@-webkit-keyframes opacity_loop{0%{opacity:0}30%{opacity:1}60%{opacity:1}90%{opacity:0}}@keyframes opacity_loop{0%{opacity:0}30%{opacity:1}60%{opacity:1}90%{opacity:0}}";
//       echo '<style>'.$css_File.'</style>';
//       echo '<!-- '.$handle.' ends -->';
//   	}else{
//       array_push($GLOBALS['all_styles'],[$handle,$src,$media]);
//     }

//   	return;
//   }else{
//     return $tag;
//   }
// }

?>

