var main_banner_owl;
var main_banner_timeout;
function checkOwl(){
  if($.fn.owlCarousel) {
    owlInit();
  }else{
    setTimeout(checkOwl,500);
  }
}
setTimeout(checkOwl, 500);

function owlInit(){

  main_banner_owl = $('.owl-carousel').owlCarousel({
    lazyLoad: true,
    loop:true,
    margin:0,
    nav: true,
    dots: false,
    navText: ["<",">"],
    responsive:{
      0:{
        items:1
      },
      600:{
        items:2
      },
      1300:{
        items:window.jscount
      },
      1600:{
        items:window.jscount
      }
    }
  })
}