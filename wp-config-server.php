<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'primedigtydev1' );

/** MySQL database username */
define( 'DB_USER', 'primedigtydev1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'develoP1PD' );

/** MySQL hostname */
define( 'DB_HOST', 'primedigtydev1.mysql.db' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'f1biv995s;$%l<_^7=?5A[nT]BPf +lBt;f4BiLS(>&XDt3qEFHI1L7NnxW3d!Ns' );
define( 'SECURE_AUTH_KEY',  'FuR-.|9`apyl_bYbl2pO{)Xcv7E@!$_R&/8=$^B/kdNcDyNqy9(fEqX(Mxc!7-Yg' );
define( 'LOGGED_IN_KEY',    'd|!D@fram9BxhWq^CQzBh)iS&!gqt$i3+$[`3c]O+yN7I?-&:0+w*]`z0ZD*Ihi(' );
define( 'NONCE_KEY',        ':x)o<JVlpxjRo).}CNakBUhJ;u%9{~nEV~;>U_,I/[zg|MD>41sk TdOg&bCWq}S' );
define( 'AUTH_SALT',        'jc:(pb5!T%~6g*/FiQ0]l6iSY7E@ZyIMv]t&x:K)QLp3,t2?UYH)Ko5bpA[,uDKf' );
define( 'SECURE_AUTH_SALT', 'jb)VAycqpv38J]d{dN-vi^fR(z!Et.blJ:txr52^;OiTYI}7&xJ%vqj+2tPcwADx' );
define( 'LOGGED_IN_SALT',   'xHQ5k$T!urc:{>j$42HpwN-G*5o3y!>$SmXtF2,dKR<.7RF5#JUGSbbAH?+:^)k`' );
define( 'NONCE_SALT',       '/(5v~#hWBEBp=UV8IRv{s#N2u~$Jw,YKx?4|G@c$`p c* !zb5;XC*Npt2YrBYX,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
