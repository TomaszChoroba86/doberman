const critical = require('critical');
const gulp = require("gulp");
const cssPurge = require('css-purge');
const path = '../../cache/cache-enabler/';
// gulp.task('critical', ['build'], function (cb) {
//     critical.generate({
//         inline: false,
//         base: '../../../cache/cache-enabler/maxmobil.local.com/',
//         src: 'http-index.html',
//         // dest: 'dist/index-critical.html',
//         minify: true,
//         width: 320,
//         height: 480
//     });
// });
function purge() {
  return cssPurge.purgeCSSFiles({
      css_output: './purge/critical.min.css',
      css: './dist/'
  });
}

function critical_gen() {
  return critical.generate({
            inline: false,
            base: '../',
            src: path+'doberman.local.com/szczenieta/http-index.html',
            target: {
              css: './critical/dist/szczenieta/index.css',
            },
            minify: true,
            dimensions: [
             {
               width: 1920,
               height: 1080
             },
             {
               width: 900,
               height: 640
             },
           ],
           ignore: {
            atrule: ['@font-face'],
            // decl: (node, value) => /url\(/.test(value),
          }
        });
}


const dev = gulp.series(critical_gen);

// export tasks;
exports.purge = purge;
exports.default = dev;
