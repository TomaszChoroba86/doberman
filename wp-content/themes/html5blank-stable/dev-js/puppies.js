if (window.jscount==undefined) {
  window.jscount = 3;
} 

$('.owl-carousel').owlCarousel({
  lazyLoad: true,
  loop:true,
  margin:0,
  nav: true,
  dots: false,
  navText: ["<",">"],
  responsive:{
    0:{
      items:1
    },
    600:{
      items:2
    },
    1300:{
      items:window.jscount
    },
    1600:{
      items:window.jscount
    }
  }
})
