-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 12 Paź 2020, 12:02
-- Wersja serwera: 10.4.13-MariaDB
-- Wersja PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `doberman`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Komentator WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-10-01 09:53:01', '2020-10-01 07:53:01', 'Cześć, to jest komentarz.\nAby zapoznać się z moderowaniem, edycją i usuwaniem komentarzy, należy odwiedzić ekran Komentarze w kokpicie.\nAwatary komentujących pochodzą z <a href=\"https://pl.gravatar.com\">Gravatara</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://doberman.local.com', 'yes'),
(2, 'home', 'http://doberman.local.com', 'yes'),
(3, 'blogname', 'Doberman Project', 'yes'),
(4, 'blogdescription', 'Kolejna witryna oparta na WordPressie', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'prime.dev.work@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:131:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:14:\"html5-blank/?$\";s:31:\"index.php?post_type=html5-blank\";s:44:\"html5-blank/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=html5-blank&feed=$matches[1]\";s:39:\"html5-blank/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=html5-blank&feed=$matches[1]\";s:31:\"html5-blank/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=html5-blank&paged=$matches[1]\";s:10:\"puppies/?$\";s:27:\"index.php?post_type=puppies\";s:40:\"puppies/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=puppies&feed=$matches[1]\";s:35:\"puppies/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=puppies&feed=$matches[1]\";s:27:\"puppies/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=puppies&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:37:\"html5-blank/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"html5-blank/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"html5-blank/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"html5-blank/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"html5-blank/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"html5-blank/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"html5-blank/(.+?)/embed/?$\";s:44:\"index.php?html5-blank=$matches[1]&embed=true\";s:30:\"html5-blank/(.+?)/trackback/?$\";s:38:\"index.php?html5-blank=$matches[1]&tb=1\";s:50:\"html5-blank/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?html5-blank=$matches[1]&feed=$matches[2]\";s:45:\"html5-blank/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?html5-blank=$matches[1]&feed=$matches[2]\";s:38:\"html5-blank/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?html5-blank=$matches[1]&paged=$matches[2]\";s:45:\"html5-blank/(.+?)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?html5-blank=$matches[1]&cpage=$matches[2]\";s:34:\"html5-blank/(.+?)(?:/([0-9]+))?/?$\";s:50:\"index.php?html5-blank=$matches[1]&page=$matches[2]\";s:33:\"puppies/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"puppies/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"puppies/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"puppies/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"puppies/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"puppies/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"puppies/(.+?)/embed/?$\";s:40:\"index.php?puppies=$matches[1]&embed=true\";s:26:\"puppies/(.+?)/trackback/?$\";s:34:\"index.php?puppies=$matches[1]&tb=1\";s:46:\"puppies/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?puppies=$matches[1]&feed=$matches[2]\";s:41:\"puppies/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?puppies=$matches[1]&feed=$matches[2]\";s:34:\"puppies/(.+?)/page/?([0-9]{1,})/?$\";s:47:\"index.php?puppies=$matches[1]&paged=$matches[2]\";s:41:\"puppies/(.+?)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?puppies=$matches[1]&cpage=$matches[2]\";s:30:\"puppies/(.+?)(?:/([0-9]+))?/?$\";s:46:\"index.php?puppies=$matches[1]&page=$matches[2]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=51&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:30:\"advanced-custom-fields/acf.php\";i:2;s:53:\"simple-custom-post-order/simple-custom-post-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:76:\"C:\\xampp\\htdocs\\doberman/wp-content/plugins/acf-duplicate-repeater/index.php\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'html5blank-stable', 'yes'),
(41, 'stylesheet', 'html5blank-stable', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '48748', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:1:{s:53:\"simple-custom-post-order/simple-custom-post-order.php\";s:18:\"scporder_uninstall\";}', 'no'),
(80, 'timezone_string', 'Europe/Warsaw', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '51', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1617090780', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'initial_db_version', '48748', 'yes'),
(96, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'WPLANG', 'pl_PL', 'yes'),
(99, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"widget-area-1\";a:0:{}s:13:\"widget-area-2\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(105, 'cron', 'a:7:{i:1602496382;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1602532382;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1602575581;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602575592;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602575594;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602834782;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(106, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'recovery_keys', 'a:0:{}', 'yes'),
(117, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"pl_PL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pl_PL/wordpress-5.5.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1602489324;s:15:\"version_checked\";s:5:\"5.5.1\";s:12:\"translations\";a:0:{}}', 'no'),
(119, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1601539250;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(123, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1602489325;s:7:\"checked\";a:1:{s:17:\"html5blank-stable\";s:5:\"1.4.3\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(131, 'can_compress_scripts', '1', 'no'),
(144, 'finished_updating_comment_type', '1', 'yes'),
(145, 'current_theme', 'HTML5 Blank', 'yes'),
(146, 'theme_mods_html5blank-stable', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:13:\"doberman-menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(147, 'theme_switched', '', 'yes'),
(151, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:1:{i:0;i:2;}}', 'yes'),
(155, 'recently_activated', 'a:0:{}', 'yes'),
(172, '_transient_health-check-site-status-result', '{\"good\":\"11\",\"recommended\":\"9\",\"critical\":\"0\"}', 'yes'),
(212, 'acf_version', '5.8.7', 'yes'),
(262, 'scporder_install', '1', 'yes'),
(263, 'simple-rate-time', '1602156028', 'yes'),
(264, 'scporder_options', 'a:3:{s:7:\"objects\";a:1:{i:0;s:7:\"puppies\";}s:4:\"tags\";s:0:\"\";s:18:\"show_advanced_view\";s:0:\"\";}', 'yes'),
(266, 'category_children', 'a:0:{}', 'yes'),
(290, '_site_transient_timeout_browser_ce4e9e986b0fbc713624d54b83c36283', '1602759793', 'no'),
(291, '_site_transient_browser_ce4e9e986b0fbc713624d54b83c36283', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"85.0.4183.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(292, '_site_transient_timeout_php_check_3dbf48b9658abaee82651209c2ca7be3', '1602759793', 'no'),
(293, '_site_transient_php_check_3dbf48b9658abaee82651209c2ca7be3', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(346, '_transient_timeout_acf_plugin_updates', '1602607696', 'no'),
(347, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.5.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.7\";}}', 'no'),
(374, '_site_transient_timeout_theme_roots', '1602491125', 'no'),
(375, '_site_transient_theme_roots', 'a:1:{s:17:\"html5blank-stable\";s:7:\"/themes\";}', 'no'),
(376, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1602489326;s:7:\"checked\";a:5:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.9.1\";s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.7\";s:19:\"akismet/akismet.php\";s:5:\"4.1.6\";s:9:\"hello.php\";s:5:\"1.7.2\";s:53:\"simple-custom-post-order/simple-custom-post-order.php\";s:5:\"2.5.1\";}s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.5.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.1\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:53:\"simple-custom-post-order/simple-custom-post-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:38:\"w.org/plugins/simple-custom-post-order\";s:4:\"slug\";s:24:\"simple-custom-post-order\";s:6:\"plugin\";s:53:\"simple-custom-post-order/simple-custom-post-order.php\";s:11:\"new_version\";s:5:\"2.5.1\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/simple-custom-post-order/\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/plugin/simple-custom-post-order.2.5.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/simple-custom-post-order/assets/icon-256x256.jpg?rev=1859717\";s:2:\"1x\";s:77:\"https://ps.w.org/simple-custom-post-order/assets/icon-256x256.jpg?rev=1859717\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:79:\"https://ps.w.org/simple-custom-post-order/assets/banner-772x250.jpg?rev=1859717\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'template-glowna.php'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_menu_item_type', 'custom'),
(4, 5, '_menu_item_menu_item_parent', '0'),
(5, 5, '_menu_item_object_id', '5'),
(6, 5, '_menu_item_object', 'custom'),
(7, 5, '_menu_item_target', ''),
(8, 5, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(9, 5, '_menu_item_xfn', ''),
(10, 5, '_menu_item_url', 'http://xxx'),
(12, 6, '_menu_item_type', 'custom'),
(13, 6, '_menu_item_menu_item_parent', '0'),
(14, 6, '_menu_item_object_id', '6'),
(15, 6, '_menu_item_object', 'custom'),
(16, 6, '_menu_item_target', ''),
(17, 6, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(18, 6, '_menu_item_xfn', ''),
(19, 6, '_menu_item_url', 'http://xxx'),
(21, 7, '_menu_item_type', 'custom'),
(22, 7, '_menu_item_menu_item_parent', '0'),
(23, 7, '_menu_item_object_id', '7'),
(24, 7, '_menu_item_object', 'custom'),
(25, 7, '_menu_item_target', ''),
(26, 7, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(27, 7, '_menu_item_xfn', ''),
(28, 7, '_menu_item_url', 'http://xxx'),
(30, 8, '_menu_item_type', 'custom'),
(31, 8, '_menu_item_menu_item_parent', '0'),
(32, 8, '_menu_item_object_id', '8'),
(33, 8, '_menu_item_object', 'custom'),
(34, 8, '_menu_item_target', ''),
(35, 8, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(36, 8, '_menu_item_xfn', ''),
(37, 8, '_menu_item_url', 'http://xxx'),
(39, 9, '_menu_item_type', 'custom'),
(40, 9, '_menu_item_menu_item_parent', '0'),
(41, 9, '_menu_item_object_id', '9'),
(42, 9, '_menu_item_object', 'custom'),
(43, 9, '_menu_item_target', ''),
(44, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(45, 9, '_menu_item_xfn', ''),
(46, 9, '_menu_item_url', 'http://xxx'),
(48, 10, '_menu_item_type', 'custom'),
(49, 10, '_menu_item_menu_item_parent', '0'),
(50, 10, '_menu_item_object_id', '10'),
(51, 10, '_menu_item_object', 'custom'),
(52, 10, '_menu_item_target', ''),
(53, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(54, 10, '_menu_item_xfn', ''),
(55, 10, '_menu_item_url', 'http://xxx'),
(57, 11, '_menu_item_type', 'custom'),
(58, 11, '_menu_item_menu_item_parent', '0'),
(59, 11, '_menu_item_object_id', '11'),
(60, 11, '_menu_item_object', 'custom'),
(61, 11, '_menu_item_target', ''),
(62, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(63, 11, '_menu_item_xfn', ''),
(64, 11, '_menu_item_url', 'http://xxx'),
(66, 12, '_menu_item_type', 'custom'),
(67, 12, '_menu_item_menu_item_parent', '6'),
(68, 12, '_menu_item_object_id', '12'),
(69, 12, '_menu_item_object', 'custom'),
(70, 12, '_menu_item_target', ''),
(71, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(72, 12, '_menu_item_xfn', ''),
(73, 12, '_menu_item_url', 'http://xxx'),
(75, 13, '_menu_item_type', 'custom'),
(76, 13, '_menu_item_menu_item_parent', '6'),
(77, 13, '_menu_item_object_id', '13'),
(78, 13, '_menu_item_object', 'custom'),
(79, 13, '_menu_item_target', ''),
(80, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(81, 13, '_menu_item_xfn', ''),
(82, 13, '_menu_item_url', 'http://xxx'),
(84, 14, '_menu_item_type', 'custom'),
(85, 14, '_menu_item_menu_item_parent', '6'),
(86, 14, '_menu_item_object_id', '14'),
(87, 14, '_menu_item_object', 'custom'),
(88, 14, '_menu_item_target', ''),
(89, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(90, 14, '_menu_item_xfn', ''),
(91, 14, '_menu_item_url', 'http://xxx'),
(93, 15, '_menu_item_type', 'custom'),
(94, 15, '_menu_item_menu_item_parent', '6'),
(95, 15, '_menu_item_object_id', '15'),
(96, 15, '_menu_item_object', 'custom'),
(97, 15, '_menu_item_target', ''),
(98, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(99, 15, '_menu_item_xfn', ''),
(100, 15, '_menu_item_url', 'http://xxx'),
(102, 16, '_wp_trash_meta_status', 'publish'),
(103, 16, '_wp_trash_meta_time', '1601543195'),
(104, 2, '_edit_lock', '1602238114:1'),
(105, 17, '_edit_last', '1'),
(106, 17, '_edit_lock', '1602231650:1'),
(107, 2, '_edit_last', '1'),
(108, 2, 'flexible_content', ''),
(109, 2, '_flexible_content', 'field_5f7b17362508c'),
(110, 26, 'flexible_content', ''),
(111, 26, '_flexible_content', 'field_5f7b17362508c'),
(112, 27, '_edit_lock', '1601906658:1'),
(113, 27, '_edit_last', '1'),
(114, 36, '_edit_lock', '1601904795:1'),
(115, 2, 'blackbanner_image', ''),
(116, 2, '_blackbanner_image', 'field_5f7b2055b6a2f'),
(117, 26, 'blackbanner_image', ''),
(118, 26, '_blackbanner_image', 'field_5f7b2055b6a2f'),
(119, 37, '_wp_attached_file', '2020/10/Doberman.png'),
(120, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:308;s:6:\"height\";i:343;s:4:\"file\";s:20:\"2020/10/Doberman.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Doberman-250x278.png\";s:5:\"width\";i:250;s:6:\"height\";i:278;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Doberman-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"small\";a:4:{s:4:\"file\";s:20:\"Doberman-120x134.png\";s:5:\"width\";i:120;s:6:\"height\";i:134;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:20:\"Doberman-308x200.png\";s:5:\"width\";i:308;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 38, '_wp_attached_file', '2020/10/Warstwa-18.jpg'),
(122, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1057;s:6:\"height\";i:539;s:4:\"file\";s:22:\"2020/10/Warstwa-18.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Warstwa-18-250x127.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:127;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"Warstwa-18-700x357.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Warstwa-18-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"Warstwa-18-768x392.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:392;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:21:\"Warstwa-18-120x61.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:61;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:22:\"Warstwa-18-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(131, 39, 'flexible_content', ''),
(132, 39, '_flexible_content', 'field_5f7b17362508c'),
(133, 39, 'blackbanner_image', 'a:1:{i:0;s:7:\"content\";}'),
(134, 39, '_blackbanner_image', 'field_5f7b2055b6a2f'),
(135, 39, 'blackbanner_image_0_section_0_col_1', '<img class=\"alignnone size-medium wp-image-37\" src=\"http://doberman.local.com/wp-content/uploads/2020/10/Doberman-250x278.png\" alt=\"\" />'),
(136, 39, '_blackbanner_image_0_section_0_col_1', 'field_5f7b2055c9106'),
(137, 39, 'blackbanner_image_0_section_0_col_2', 'test'),
(138, 39, '_blackbanner_image_0_section_0_col_2', 'field_5f7b2055ccd25'),
(139, 39, 'blackbanner_image_0_section_0_col_3', '<img class=\"alignnone size-medium wp-image-38\" src=\"http://doberman.local.com/wp-content/uploads/2020/10/Warstwa-18-250x127.jpg\" alt=\"\" />'),
(140, 39, '_blackbanner_image_0_section_0_col_3', 'field_5f7b2055d0771'),
(141, 39, 'blackbanner_image_0_section', 'a:1:{i:0;s:9:\"columns_3\";}'),
(142, 39, '_blackbanner_image_0_section', 'field_5f7b2055ba7ea'),
(143, 40, 'flexible_content', ''),
(144, 40, '_flexible_content', 'field_5f7b17362508c'),
(145, 40, 'blackbanner_image', ''),
(146, 40, '_blackbanner_image', 'field_5f7b2055b6a2f'),
(147, 45, '_edit_last', '1'),
(148, 45, '_edit_lock', '1602230493:1'),
(149, 27, '_wp_trash_meta_status', 'publish'),
(150, 27, '_wp_trash_meta_time', '1601906924'),
(151, 27, '_wp_desired_post_slug', 'group_5f7b2055a0e6c'),
(152, 28, '_wp_trash_meta_status', 'publish'),
(153, 28, '_wp_trash_meta_time', '1601906924'),
(154, 28, '_wp_desired_post_slug', 'field_5f7b2055b6a2f'),
(155, 51, '_edit_lock', '1602445747:1'),
(164, 51, '_edit_last', '1'),
(165, 51, 'image_left', '37'),
(166, 51, '_image_left', 'field_5f7b2882aaebf'),
(167, 51, 'titile_text', 'test'),
(168, 51, '_titile_text', 'field_5f7b2897aaec0'),
(169, 51, 'descritpion_text', 'test'),
(170, 51, '_descritpion_text', 'field_5f7b28aaaaec1'),
(171, 51, 'button_text', 'test'),
(172, 51, '_button_text', 'field_5f7b28b7aaec2'),
(173, 51, 'right_image', '38'),
(174, 51, '_right_image', 'field_5f7b28c7aaec3'),
(175, 51, 'flexible_content', ''),
(176, 51, '_flexible_content', 'field_5f7b17362508c'),
(177, 54, 'image_left', '37'),
(178, 54, '_image_left', 'field_5f7b2882aaebf'),
(179, 54, 'titile_text', 'test'),
(180, 54, '_titile_text', 'field_5f7b2897aaec0'),
(181, 54, 'descritpion_text', 'test'),
(182, 54, '_descritpion_text', 'field_5f7b28aaaaec1'),
(183, 54, 'button_text', 'test'),
(184, 54, '_button_text', 'field_5f7b28b7aaec2'),
(185, 54, 'right_image', '38'),
(186, 54, '_right_image', 'field_5f7b28c7aaec3'),
(187, 54, 'flexible_content', ''),
(188, 54, '_flexible_content', 'field_5f7b17362508c'),
(189, 51, '_wp_page_template', 'template-glowna.php'),
(190, 55, '_edit_last', '1'),
(191, 55, '_edit_lock', '1602057680:1'),
(192, 56, '_wp_attached_file', '2020/10/pup1.jpg'),
(193, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:479;s:6:\"height\";i:796;s:4:\"file\";s:16:\"2020/10/pup1.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"pup1-250x415.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"pup1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"pup1-120x199.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:16:\"pup1-479x200.jpg\";s:5:\"width\";i:479;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(194, 55, '_thumbnail_id', '56'),
(195, 57, '_edit_last', '1'),
(196, 57, '_edit_lock', '1602057713:1'),
(197, 58, '_wp_attached_file', '2020/10/pup2.jpg'),
(198, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:482;s:6:\"height\";i:796;s:4:\"file\";s:16:\"2020/10/pup2.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"pup2-250x413.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:413;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"pup2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"pup2-120x198.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:198;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:16:\"pup2-482x200.jpg\";s:5:\"width\";i:482;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(199, 57, '_thumbnail_id', '58'),
(200, 59, '_edit_last', '1'),
(201, 59, '_edit_lock', '1602058439:1'),
(202, 60, '_wp_attached_file', '2020/10/pup3.jpg'),
(203, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:481;s:6:\"height\";i:796;s:4:\"file\";s:16:\"2020/10/pup3.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"pup3-250x414.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:414;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"pup3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"pup3-120x199.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:16:\"pup3-481x200.jpg\";s:5:\"width\";i:481;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(204, 59, '_thumbnail_id', '60'),
(205, 61, '_edit_last', '1'),
(206, 61, '_edit_lock', '1602060602:1'),
(207, 62, '_wp_attached_file', '2020/10/pup4.jpg'),
(208, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:482;s:6:\"height\";i:796;s:4:\"file\";s:16:\"2020/10/pup4.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"pup4-250x413.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:413;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"pup4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"pup4-120x198.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:198;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:16:\"pup4-482x200.jpg\";s:5:\"width\";i:482;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(209, 61, '_thumbnail_id', '62'),
(210, 64, '_wp_attached_file', '2020/10/pup1-1.jpg'),
(211, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:479;s:6:\"height\";i:796;s:4:\"file\";s:18:\"2020/10/pup1-1.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"pup1-1-250x415.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"pup1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:18:\"pup1-1-120x199.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:18:\"pup1-1-479x200.jpg\";s:5:\"width\";i:479;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(212, 63, '_edit_last', '1'),
(213, 63, '_thumbnail_id', '64'),
(214, 63, '_edit_lock', '1602072212:1'),
(215, 65, '_edit_last', '1'),
(216, 65, '_edit_lock', '1602072225:1'),
(217, 65, '_thumbnail_id', '58'),
(218, 66, '_edit_last', '1'),
(219, 66, '_edit_lock', '1602072234:1'),
(220, 66, '_thumbnail_id', '60'),
(221, 67, '_edit_last', '1'),
(222, 67, '_edit_lock', '1602073681:1'),
(223, 67, '_thumbnail_id', '62'),
(224, 68, '_edit_last', '1'),
(225, 68, '_edit_lock', '1602230042:1'),
(226, 63, 'puppies_descr', 'Lorem ipsum dolor sit tetur sed'),
(227, 63, '_puppies_descr', 'field_5f7dadfd56d5f'),
(228, 65, 'puppies_descr', 'Lorem ipsum dolor sit tetur sed'),
(229, 65, '_puppies_descr', 'field_5f7dadfd56d5f'),
(230, 66, 'puppies_descr', 'Lorem ipsum dolor sit tetur sed'),
(231, 66, '_puppies_descr', 'field_5f7dadfd56d5f'),
(232, 67, 'puppies_descr', 'Lorem ipsum dolor sit tetur sed'),
(233, 67, '_puppies_descr', 'field_5f7dadfd56d5f'),
(234, 70, '_edit_last', '1'),
(235, 70, '_edit_lock', '1602074932:1'),
(236, 71, '_wp_attached_file', '2020/10/pup5.jpg'),
(237, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:482;s:6:\"height\";i:796;s:4:\"file\";s:16:\"2020/10/pup5.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"pup5-250x413.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:413;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"pup5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"pup5-120x198.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:198;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:16:\"pup5-482x200.jpg\";s:5:\"width\";i:482;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(238, 70, '_thumbnail_id', '71'),
(239, 70, 'puppies_descr', 'Lorem ipsum dolor sit tetur sed'),
(240, 70, '_puppies_descr', 'field_5f7dadfd56d5f'),
(245, 75, '_edit_lock', '1602251767:1'),
(246, 75, '_edit_last', '1'),
(247, 45, '_wp_trash_meta_status', 'publish'),
(248, 45, '_wp_trash_meta_time', '1602230643'),
(249, 45, '_wp_desired_post_slug', 'group_5f7b287584ab8'),
(250, 46, '_wp_trash_meta_status', 'publish'),
(251, 46, '_wp_trash_meta_time', '1602230643'),
(252, 46, '_wp_desired_post_slug', 'field_5f7b2882aaebf'),
(253, 47, '_wp_trash_meta_status', 'publish'),
(254, 47, '_wp_trash_meta_time', '1602230644'),
(255, 47, '_wp_desired_post_slug', 'field_5f7b2897aaec0'),
(256, 48, '_wp_trash_meta_status', 'publish'),
(257, 48, '_wp_trash_meta_time', '1602230644'),
(258, 48, '_wp_desired_post_slug', 'field_5f7b28aaaaec1'),
(259, 49, '_wp_trash_meta_status', 'publish'),
(260, 49, '_wp_trash_meta_time', '1602230644'),
(261, 49, '_wp_desired_post_slug', 'field_5f7b28b7aaec2'),
(262, 50, '_wp_trash_meta_status', 'publish'),
(263, 50, '_wp_trash_meta_time', '1602230644'),
(264, 50, '_wp_desired_post_slug', 'field_5f7b28c7aaec3'),
(265, 88, '_wp_attached_file', '2020/10/dob03.jpg'),
(266, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:814;s:4:\"file\";s:17:\"2020/10/dob03.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"dob03-250x106.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:106;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"dob03-700x297.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:297;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"dob03-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"dob03-768x326.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:326;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"dob03-1536x651.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:651;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"dob03-120x51.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:51;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"dob03-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(275, 51, 'components_0_section', 'a:4:{i:0;s:11:\"component_2\";i:1;s:11:\"component_1\";i:2;s:11:\"component_2\";i:3;s:11:\"component_1\";}'),
(276, 51, '_components_0_section', 'field_5f80182dbac72'),
(277, 51, 'components', 'a:1:{i:0;s:7:\"content\";}'),
(278, 51, '_components', 'field_5f80182db7019'),
(279, 89, 'image_left', '37'),
(280, 89, '_image_left', 'field_5f7b2882aaebf'),
(281, 89, 'titile_text', 'test'),
(282, 89, '_titile_text', 'field_5f7b2897aaec0'),
(283, 89, 'descritpion_text', 'test'),
(284, 89, '_descritpion_text', 'field_5f7b28aaaaec1'),
(285, 89, 'button_text', 'test'),
(286, 89, '_button_text', 'field_5f7b28b7aaec2'),
(287, 89, 'right_image', '38'),
(288, 89, '_right_image', 'field_5f7b28c7aaec3'),
(289, 89, 'flexible_content', ''),
(290, 89, '_flexible_content', 'field_5f7b17362508c'),
(291, 89, 'components_0_section_0_title', 'O rasie'),
(292, 89, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(293, 89, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(294, 89, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(295, 89, 'components_0_section_0_button_text', 'Zobacz'),
(296, 89, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(297, 89, 'components_0_section_0_background', '88'),
(298, 89, '_components_0_section_0_background', 'field_5f80190cefe25'),
(299, 89, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(300, 89, '_components_0_section', 'field_5f80182dbac72'),
(301, 89, 'components', 'a:1:{i:0;s:7:\"content\";}'),
(302, 89, '_components', 'field_5f80182db7019'),
(303, 90, 'image_left', '37'),
(304, 90, '_image_left', 'field_5f7b2882aaebf'),
(305, 90, 'titile_text', 'test'),
(306, 90, '_titile_text', 'field_5f7b2897aaec0'),
(307, 90, 'descritpion_text', 'test'),
(308, 90, '_descritpion_text', 'field_5f7b28aaaaec1'),
(309, 90, 'button_text', 'test'),
(310, 90, '_button_text', 'field_5f7b28b7aaec2'),
(311, 90, 'right_image', '38'),
(312, 90, '_right_image', 'field_5f7b28c7aaec3'),
(313, 90, 'flexible_content', ''),
(314, 90, '_flexible_content', 'field_5f7b17362508c'),
(315, 90, 'components_0_section_0_title', 'O rasie'),
(316, 90, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(317, 90, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(318, 90, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(319, 90, 'components_0_section_0_button_text', 'Zobacz'),
(320, 90, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(321, 90, 'components_0_section_0_background', '88'),
(322, 90, '_components_0_section_0_background', 'field_5f80190cefe25'),
(323, 90, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(324, 90, '_components_0_section', 'field_5f80182dbac72'),
(325, 90, 'components', 'a:1:{i:0;s:7:\"content\";}'),
(326, 90, '_components', 'field_5f80182db7019'),
(327, 91, 'image_left', '37'),
(328, 91, '_image_left', 'field_5f7b2882aaebf'),
(329, 91, 'titile_text', 'test'),
(330, 91, '_titile_text', 'field_5f7b2897aaec0'),
(331, 91, 'descritpion_text', 'test'),
(332, 91, '_descritpion_text', 'field_5f7b28aaaaec1'),
(333, 91, 'button_text', 'test'),
(334, 91, '_button_text', 'field_5f7b28b7aaec2'),
(335, 91, 'right_image', '38'),
(336, 91, '_right_image', 'field_5f7b28c7aaec3'),
(337, 91, 'flexible_content', ''),
(338, 91, '_flexible_content', 'field_5f7b17362508c'),
(339, 91, 'components_0_section_0_title', 'O rasie'),
(340, 91, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(341, 91, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(342, 91, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(343, 91, 'components_0_section_0_button_text', 'Zobacz'),
(344, 91, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(345, 91, 'components_0_section_0_background', '88'),
(346, 91, '_components_0_section_0_background', 'field_5f80190cefe25'),
(347, 91, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(348, 91, '_components_0_section', 'field_5f80182dbac72'),
(349, 91, 'components', 'a:1:{i:0;s:7:\"content\";}'),
(350, 91, '_components', 'field_5f80182db7019'),
(359, 92, 'image_left', '37'),
(360, 92, '_image_left', 'field_5f7b2882aaebf'),
(361, 92, 'titile_text', 'test'),
(362, 92, '_titile_text', 'field_5f7b2897aaec0'),
(363, 92, 'descritpion_text', 'test'),
(364, 92, '_descritpion_text', 'field_5f7b28aaaaec1'),
(365, 92, 'button_text', 'test'),
(366, 92, '_button_text', 'field_5f7b28b7aaec2'),
(367, 92, 'right_image', '38'),
(368, 92, '_right_image', 'field_5f7b28c7aaec3'),
(369, 92, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(370, 92, '_flexible_content', 'field_5f7b17362508c'),
(371, 92, 'components_0_section_0_title', 'O rasie'),
(372, 92, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(373, 92, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(374, 92, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(375, 92, 'components_0_section_0_button_text', 'Zobacz'),
(376, 92, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(377, 92, 'components_0_section_0_background', '88'),
(378, 92, '_components_0_section_0_background', 'field_5f80190cefe25'),
(379, 92, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(380, 92, '_components_0_section', 'field_5f80182dbac72'),
(381, 92, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(382, 92, '_components', 'field_5f80182db7019'),
(383, 92, 'components_1_section', ''),
(384, 92, '_components_1_section', 'field_5f80182dbac72'),
(385, 92, 'flexible_content_0_section_0_col_1', 'test'),
(386, 92, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(387, 92, 'flexible_content_0_section_0_col_2', 'test'),
(388, 92, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(389, 92, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(390, 92, '_flexible_content_0_section', 'field_5f7b17562508d'),
(391, 93, 'image_left', '37'),
(392, 93, '_image_left', 'field_5f7b2882aaebf'),
(393, 93, 'titile_text', 'test'),
(394, 93, '_titile_text', 'field_5f7b2897aaec0'),
(395, 93, 'descritpion_text', 'test'),
(396, 93, '_descritpion_text', 'field_5f7b28aaaaec1'),
(397, 93, 'button_text', 'test'),
(398, 93, '_button_text', 'field_5f7b28b7aaec2'),
(399, 93, 'right_image', '38'),
(400, 93, '_right_image', 'field_5f7b28c7aaec3'),
(401, 93, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(402, 93, '_flexible_content', 'field_5f7b17362508c'),
(403, 93, 'components_0_section_0_title', 'O rasie'),
(404, 93, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(405, 93, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(406, 93, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(407, 93, 'components_0_section_0_button_text', 'Zobacz'),
(408, 93, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(409, 93, 'components_0_section_0_background', '88'),
(410, 93, '_components_0_section_0_background', 'field_5f80190cefe25'),
(411, 93, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(412, 93, '_components_0_section', 'field_5f80182dbac72'),
(413, 93, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(414, 93, '_components', 'field_5f80182db7019'),
(415, 93, 'components_1_section', ''),
(416, 93, '_components_1_section', 'field_5f80182dbac72'),
(417, 93, 'flexible_content_0_section_0_col_1', 'test'),
(418, 93, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(419, 93, 'flexible_content_0_section_0_col_2', 'test'),
(420, 93, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(421, 93, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(422, 93, '_flexible_content_0_section', 'field_5f7b17562508d'),
(423, 95, 'image_left', '37'),
(424, 95, '_image_left', 'field_5f7b2882aaebf'),
(425, 95, 'titile_text', 'test'),
(426, 95, '_titile_text', 'field_5f7b2897aaec0'),
(427, 95, 'descritpion_text', 'test'),
(428, 95, '_descritpion_text', 'field_5f7b28aaaaec1'),
(429, 95, 'button_text', 'test'),
(430, 95, '_button_text', 'field_5f7b28b7aaec2'),
(431, 95, 'right_image', '38'),
(432, 95, '_right_image', 'field_5f7b28c7aaec3'),
(433, 95, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(434, 95, '_flexible_content', 'field_5f7b17362508c'),
(435, 95, 'components_0_section_0_title', 'O rasie'),
(436, 95, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(437, 95, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(438, 95, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(439, 95, 'components_0_section_0_button_text', 'Zobacz'),
(440, 95, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(441, 95, 'components_0_section_0_background', '88'),
(442, 95, '_components_0_section_0_background', 'field_5f80190cefe25'),
(443, 95, 'components_0_section', 'a:1:{i:0;s:11:\"component_1\";}'),
(444, 95, '_components_0_section', 'field_5f80182dbac72'),
(445, 95, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(446, 95, '_components', 'field_5f80182db7019'),
(447, 95, 'components_1_section', ''),
(448, 95, '_components_1_section', 'field_5f80182dbac72'),
(449, 95, 'flexible_content_0_section_0_col_1', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(450, 95, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(451, 95, 'flexible_content_0_section_0_col_2', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(452, 95, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(453, 95, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(454, 95, '_flexible_content_0_section', 'field_5f7b17562508d'),
(455, 96, '_wp_trash_meta_status', 'publish'),
(456, 96, '_wp_trash_meta_time', '1602238310'),
(457, 97, '_wp_attached_file', '2020/10/dob04.jpg'),
(458, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:773;s:4:\"file\";s:17:\"2020/10/dob04.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"dob04-250x101.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:101;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"dob04-700x282.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:282;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"dob04-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"dob04-768x309.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:309;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"dob04-1536x618.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:618;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"dob04-120x48.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:48;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"dob04-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(459, 51, 'components_0_section_1_title', 'Szczenięta'),
(460, 51, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(461, 51, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(462, 51, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(463, 51, 'components_0_section_1_button_text', 'Zobacz'),
(464, 51, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(465, 51, 'components_0_section_1_background', '88'),
(466, 51, '_components_0_section_1_background', 'field_5f80190cefe25'),
(467, 98, 'image_left', '37'),
(468, 98, '_image_left', 'field_5f7b2882aaebf'),
(469, 98, 'titile_text', 'test'),
(470, 98, '_titile_text', 'field_5f7b2897aaec0'),
(471, 98, 'descritpion_text', 'test'),
(472, 98, '_descritpion_text', 'field_5f7b28aaaaec1'),
(473, 98, 'button_text', 'test'),
(474, 98, '_button_text', 'field_5f7b28b7aaec2'),
(475, 98, 'right_image', '38'),
(476, 98, '_right_image', 'field_5f7b28c7aaec3'),
(477, 98, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(478, 98, '_flexible_content', 'field_5f7b17362508c'),
(479, 98, 'components_0_section_0_title', 'Szczenięta'),
(480, 98, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(481, 98, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(482, 98, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(483, 98, 'components_0_section_0_button_text', 'Zobacz'),
(484, 98, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(485, 98, 'components_0_section_0_background', '88'),
(486, 98, '_components_0_section_0_background', 'field_5f80190cefe25'),
(487, 98, 'components_0_section', 'a:2:{i:0;s:11:\"component_1\";i:1;s:11:\"component_1\";}'),
(488, 98, '_components_0_section', 'field_5f80182dbac72'),
(489, 98, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(490, 98, '_components', 'field_5f80182db7019'),
(491, 98, 'components_1_section', ''),
(492, 98, '_components_1_section', 'field_5f80182dbac72'),
(493, 98, 'flexible_content_0_section_0_col_1', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(494, 98, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(495, 98, 'flexible_content_0_section_0_col_2', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(496, 98, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(497, 98, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(498, 98, '_flexible_content_0_section', 'field_5f7b17562508d'),
(499, 98, 'components_0_section_1_title', 'Trening i szkolenia'),
(500, 98, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(501, 98, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(502, 98, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(503, 98, 'components_0_section_1_button_text', 'Zobacz'),
(504, 98, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(505, 98, 'components_0_section_1_background', '97'),
(506, 98, '_components_0_section_1_background', 'field_5f80190cefe25'),
(507, 99, 'image_left', '37'),
(508, 99, '_image_left', 'field_5f7b2882aaebf'),
(509, 99, 'titile_text', 'test'),
(510, 99, '_titile_text', 'field_5f7b2897aaec0'),
(511, 99, 'descritpion_text', 'test'),
(512, 99, '_descritpion_text', 'field_5f7b28aaaaec1'),
(513, 99, 'button_text', 'test'),
(514, 99, '_button_text', 'field_5f7b28b7aaec2'),
(515, 99, 'right_image', '38'),
(516, 99, '_right_image', 'field_5f7b28c7aaec3'),
(517, 99, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(518, 99, '_flexible_content', 'field_5f7b17362508c'),
(519, 99, 'components_0_section_0_title', 'Szczenięta'),
(520, 99, '_components_0_section_0_title', 'field_5f8018c8efe22'),
(521, 99, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(522, 99, '_components_0_section_0_description', 'field_5f8018dfefe23'),
(523, 99, 'components_0_section_0_button_text', 'Zobacz'),
(524, 99, '_components_0_section_0_button_text', 'field_5f8018f0efe24'),
(525, 99, 'components_0_section_0_background', '88'),
(526, 99, '_components_0_section_0_background', 'field_5f80190cefe25'),
(527, 99, 'components_0_section', 'a:2:{i:0;s:11:\"component_1\";i:1;s:11:\"component_1\";}'),
(528, 99, '_components_0_section', 'field_5f80182dbac72'),
(529, 99, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(530, 99, '_components', 'field_5f80182db7019'),
(531, 99, 'components_1_section', ''),
(532, 99, '_components_1_section', 'field_5f80182dbac72'),
(533, 99, 'flexible_content_0_section_0_col_1', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(534, 99, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(535, 99, 'flexible_content_0_section_0_col_2', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(536, 99, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(537, 99, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(538, 99, '_flexible_content_0_section', 'field_5f7b17562508d'),
(539, 99, 'components_0_section_1_title', 'Trening i szkolenia'),
(540, 99, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(541, 99, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(542, 99, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(543, 99, 'components_0_section_1_button_text', 'Zobacz'),
(544, 99, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(545, 99, 'components_0_section_1_background', '97'),
(546, 99, '_components_0_section_1_background', 'field_5f80190cefe25'),
(547, 105, '_wp_attached_file', '2020/10/dob01.jpg'),
(548, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1058;s:6:\"height\";i:539;s:4:\"file\";s:17:\"2020/10/dob01.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"dob01-250x127.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:127;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"dob01-700x357.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"dob01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"dob01-768x391.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:391;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"dob01-120x61.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:61;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"dob01-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(549, 106, '_wp_attached_file', '2020/10/dob02.jpg'),
(550, 106, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1060;s:6:\"height\";i:534;s:4:\"file\";s:17:\"2020/10/dob02.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"dob02-250x126.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:126;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"dob02-700x353.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:353;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"dob02-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"dob02-768x387.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:387;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:16:\"dob02-120x60.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:60;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"dob02-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(551, 51, 'components_0_section_0_title', 'O rasie'),
(552, 51, '_components_0_section_0_title', 'field_5f80407dfac8c'),
(553, 51, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(554, 51, '_components_0_section_0_description', 'field_5f804095fac8d'),
(555, 51, 'components_0_section_0_button_text', 'Zobacz'),
(556, 51, '_components_0_section_0_button_text', 'field_5f8040a3fac8e'),
(557, 51, 'components_0_section_0_img_right', '105'),
(558, 51, '_components_0_section_0_img_right', 'field_5f8040cefac90'),
(575, 107, 'image_left', '37'),
(576, 107, '_image_left', 'field_5f7b2882aaebf'),
(577, 107, 'titile_text', 'test'),
(578, 107, '_titile_text', 'field_5f7b2897aaec0'),
(579, 107, 'descritpion_text', 'test'),
(580, 107, '_descritpion_text', 'field_5f7b28aaaaec1'),
(581, 107, 'button_text', 'test'),
(582, 107, '_button_text', 'field_5f7b28b7aaec2'),
(583, 107, 'right_image', '38'),
(584, 107, '_right_image', 'field_5f7b28c7aaec3'),
(585, 107, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(586, 107, '_flexible_content', 'field_5f7b17362508c'),
(587, 107, 'components_0_section', 'a:4:{i:0;s:11:\"component_2\";i:1;s:11:\"component_1\";i:2;s:11:\"component_1\";i:3;s:11:\"component_2\";}'),
(588, 107, '_components_0_section', 'field_5f80182dbac72'),
(589, 107, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(590, 107, '_components', 'field_5f80182db7019'),
(591, 107, 'components_1_section', ''),
(592, 107, '_components_1_section', 'field_5f80182dbac72'),
(593, 107, 'flexible_content_0_section_0_col_1', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(594, 107, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(595, 107, 'flexible_content_0_section_0_col_2', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(596, 107, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(597, 107, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(598, 107, '_flexible_content_0_section', 'field_5f7b17562508d'),
(599, 107, 'components_0_section_1_title', 'Szczenięta'),
(600, 107, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(601, 107, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(602, 107, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(603, 107, 'components_0_section_1_button_text', 'Zobacz'),
(604, 107, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(605, 107, 'components_0_section_1_background', '88'),
(606, 107, '_components_0_section_1_background', 'field_5f80190cefe25'),
(607, 107, 'components_0_section_0_title', 'O rasie'),
(608, 107, '_components_0_section_0_title', 'field_5f80407dfac8c'),
(609, 107, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(610, 107, '_components_0_section_0_description', 'field_5f804095fac8d'),
(611, 107, 'components_0_section_0_button_text', 'Zobacz'),
(612, 107, '_components_0_section_0_button_text', 'field_5f8040a3fac8e'),
(613, 107, 'components_0_section_0_img_right', '105'),
(614, 107, '_components_0_section_0_img_right', 'field_5f8040cefac90'),
(615, 107, 'components_0_section_2_title', 'Trening i szkolenia'),
(616, 107, '_components_0_section_2_title', 'field_5f8018c8efe22'),
(617, 107, 'components_0_section_2_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(618, 107, '_components_0_section_2_description', 'field_5f8018dfefe23'),
(619, 107, 'components_0_section_2_button_text', 'Zobacz'),
(620, 107, '_components_0_section_2_button_text', 'field_5f8018f0efe24'),
(621, 107, 'components_0_section_2_background', '97'),
(622, 107, '_components_0_section_2_background', 'field_5f80190cefe25'),
(623, 107, 'components_0_section_3_title', 'Nasze dobermany'),
(624, 107, '_components_0_section_3_title', 'field_5f80407dfac8c'),
(625, 107, 'components_0_section_3_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(626, 107, '_components_0_section_3_description', 'field_5f804095fac8d'),
(627, 107, 'components_0_section_3_button_text', 'Zobacz'),
(628, 107, '_components_0_section_3_button_text', 'field_5f8040a3fac8e'),
(629, 107, 'components_0_section_3_img_right', '106'),
(630, 107, '_components_0_section_3_img_right', 'field_5f8040cefac90'),
(631, 51, 'components_0_section_2_title', 'Nasze dobermany'),
(632, 51, '_components_0_section_2_title', 'field_5f80407dfac8c'),
(633, 51, 'components_0_section_2_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(634, 51, '_components_0_section_2_description', 'field_5f804095fac8d'),
(635, 51, 'components_0_section_2_button_text', 'Zobacz'),
(636, 51, '_components_0_section_2_button_text', 'field_5f8040a3fac8e'),
(637, 51, 'components_0_section_2_img_right', '106'),
(638, 51, '_components_0_section_2_img_right', 'field_5f8040cefac90'),
(639, 51, 'components_0_section_3_title', 'Trening i szkolenia'),
(640, 51, '_components_0_section_3_title', 'field_5f8018c8efe22'),
(641, 51, 'components_0_section_3_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(642, 51, '_components_0_section_3_description', 'field_5f8018dfefe23'),
(643, 51, 'components_0_section_3_button_text', 'Zobacz'),
(644, 51, '_components_0_section_3_button_text', 'field_5f8018f0efe24'),
(645, 51, 'components_0_section_3_background', '97'),
(646, 51, '_components_0_section_3_background', 'field_5f80190cefe25'),
(647, 108, 'image_left', '37'),
(648, 108, '_image_left', 'field_5f7b2882aaebf'),
(649, 108, 'titile_text', 'test'),
(650, 108, '_titile_text', 'field_5f7b2897aaec0'),
(651, 108, 'descritpion_text', 'test'),
(652, 108, '_descritpion_text', 'field_5f7b28aaaaec1'),
(653, 108, 'button_text', 'test'),
(654, 108, '_button_text', 'field_5f7b28b7aaec2'),
(655, 108, 'right_image', '38'),
(656, 108, '_right_image', 'field_5f7b28c7aaec3'),
(657, 108, 'flexible_content', 'a:1:{i:0;s:7:\"content\";}'),
(658, 108, '_flexible_content', 'field_5f7b17362508c'),
(659, 108, 'components_0_section', 'a:4:{i:0;s:11:\"component_2\";i:1;s:11:\"component_1\";i:2;s:11:\"component_2\";i:3;s:11:\"component_1\";}'),
(660, 108, '_components_0_section', 'field_5f80182dbac72'),
(661, 108, 'components', 'a:2:{i:0;s:7:\"content\";i:1;s:7:\"content\";}'),
(662, 108, '_components', 'field_5f80182db7019'),
(663, 108, 'components_1_section', ''),
(664, 108, '_components_1_section', 'field_5f80182dbac72'),
(665, 108, 'flexible_content_0_section_0_col_1', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(666, 108, '_flexible_content_0_section_0_col_1', 'field_5f7b183025092'),
(667, 108, 'flexible_content_0_section_0_col_2', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'),
(668, 108, '_flexible_content_0_section_0_col_2', 'field_5f7b184c25093'),
(669, 108, 'flexible_content_0_section', 'a:1:{i:0;s:9:\"columns_2\";}'),
(670, 108, '_flexible_content_0_section', 'field_5f7b17562508d'),
(671, 108, 'components_0_section_1_title', 'Szczenięta'),
(672, 108, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(673, 108, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(674, 108, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(675, 108, 'components_0_section_1_button_text', 'Zobacz'),
(676, 108, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(677, 108, 'components_0_section_1_background', '88'),
(678, 108, '_components_0_section_1_background', 'field_5f80190cefe25'),
(679, 108, 'components_0_section_0_title', 'O rasie'),
(680, 108, '_components_0_section_0_title', 'field_5f80407dfac8c'),
(681, 108, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(682, 108, '_components_0_section_0_description', 'field_5f804095fac8d'),
(683, 108, 'components_0_section_0_button_text', 'Zobacz'),
(684, 108, '_components_0_section_0_button_text', 'field_5f8040a3fac8e'),
(685, 108, 'components_0_section_0_img_right', '105'),
(686, 108, '_components_0_section_0_img_right', 'field_5f8040cefac90'),
(687, 108, 'components_0_section_2_title', 'Nasze dobermany'),
(688, 108, '_components_0_section_2_title', 'field_5f80407dfac8c'),
(689, 108, 'components_0_section_2_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(690, 108, '_components_0_section_2_description', 'field_5f804095fac8d'),
(691, 108, 'components_0_section_2_button_text', 'Zobacz'),
(692, 108, '_components_0_section_2_button_text', 'field_5f8040a3fac8e'),
(693, 108, 'components_0_section_2_img_right', '106'),
(694, 108, '_components_0_section_2_img_right', 'field_5f8040cefac90'),
(695, 108, 'components_0_section_3_title', 'Trening i szkolenia'),
(696, 108, '_components_0_section_3_title', 'field_5f8018c8efe22'),
(697, 108, 'components_0_section_3_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(698, 108, '_components_0_section_3_description', 'field_5f8018dfefe23'),
(699, 108, 'components_0_section_3_button_text', 'Zobacz'),
(700, 108, '_components_0_section_3_button_text', 'field_5f8018f0efe24'),
(701, 108, 'components_0_section_3_background', '97'),
(702, 108, '_components_0_section_3_background', 'field_5f80190cefe25'),
(703, 109, 'image_left', '37'),
(704, 109, '_image_left', 'field_5f7b2882aaebf'),
(705, 109, 'titile_text', 'test'),
(706, 109, '_titile_text', 'field_5f7b2897aaec0'),
(707, 109, 'descritpion_text', 'test'),
(708, 109, '_descritpion_text', 'field_5f7b28aaaaec1'),
(709, 109, 'button_text', 'test'),
(710, 109, '_button_text', 'field_5f7b28b7aaec2'),
(711, 109, 'right_image', '38'),
(712, 109, '_right_image', 'field_5f7b28c7aaec3'),
(713, 109, 'flexible_content', ''),
(714, 109, '_flexible_content', 'field_5f7b17362508c'),
(715, 109, 'components_0_section', 'a:4:{i:0;s:11:\"component_2\";i:1;s:11:\"component_1\";i:2;s:11:\"component_2\";i:3;s:11:\"component_1\";}'),
(716, 109, '_components_0_section', 'field_5f80182dbac72'),
(717, 109, 'components', 'a:1:{i:0;s:7:\"content\";}'),
(718, 109, '_components', 'field_5f80182db7019');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(719, 109, 'components_0_section_1_title', 'Szczenięta'),
(720, 109, '_components_0_section_1_title', 'field_5f8018c8efe22'),
(721, 109, 'components_0_section_1_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(722, 109, '_components_0_section_1_description', 'field_5f8018dfefe23'),
(723, 109, 'components_0_section_1_button_text', 'Zobacz'),
(724, 109, '_components_0_section_1_button_text', 'field_5f8018f0efe24'),
(725, 109, 'components_0_section_1_background', '88'),
(726, 109, '_components_0_section_1_background', 'field_5f80190cefe25'),
(727, 109, 'components_0_section_0_title', 'O rasie'),
(728, 109, '_components_0_section_0_title', 'field_5f80407dfac8c'),
(729, 109, 'components_0_section_0_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(730, 109, '_components_0_section_0_description', 'field_5f804095fac8d'),
(731, 109, 'components_0_section_0_button_text', 'Zobacz'),
(732, 109, '_components_0_section_0_button_text', 'field_5f8040a3fac8e'),
(733, 109, 'components_0_section_0_img_right', '105'),
(734, 109, '_components_0_section_0_img_right', 'field_5f8040cefac90'),
(735, 109, 'components_0_section_2_title', 'Nasze dobermany'),
(736, 109, '_components_0_section_2_title', 'field_5f80407dfac8c'),
(737, 109, 'components_0_section_2_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(738, 109, '_components_0_section_2_description', 'field_5f804095fac8d'),
(739, 109, 'components_0_section_2_button_text', 'Zobacz'),
(740, 109, '_components_0_section_2_button_text', 'field_5f8040a3fac8e'),
(741, 109, 'components_0_section_2_img_right', '106'),
(742, 109, '_components_0_section_2_img_right', 'field_5f8040cefac90'),
(743, 109, 'components_0_section_3_title', 'Trening i szkolenia'),
(744, 109, '_components_0_section_3_title', 'field_5f8018c8efe22'),
(745, 109, 'components_0_section_3_description', 'Lorem ipsum dolor sit amet consectur adipiscing'),
(746, 109, '_components_0_section_3_description', 'field_5f8018dfefe23'),
(747, 109, 'components_0_section_3_button_text', 'Zobacz'),
(748, 109, '_components_0_section_3_button_text', 'field_5f8018f0efe24'),
(749, 109, 'components_0_section_3_background', '97'),
(750, 109, '_components_0_section_3_background', 'field_5f80190cefe25'),
(751, 1, '_edit_lock', '1602248193:1'),
(752, 111, '_wp_attached_file', '2020/10/blog1.jpg'),
(753, 111, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:932;s:6:\"height\";i:800;s:4:\"file\";s:17:\"2020/10/blog1.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog1-250x215.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:215;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"blog1-700x601.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:601;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"blog1-768x659.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:659;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:17:\"blog1-120x103.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:103;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"blog1-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(756, 1, '_thumbnail_id', '111'),
(757, 113, '_edit_lock', '1602265567:1'),
(758, 114, '_wp_attached_file', '2020/10/blog2.jpg'),
(759, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:971;s:6:\"height\";i:844;s:4:\"file\";s:17:\"2020/10/blog2.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog2-250x217.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:217;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"blog2-700x608.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:608;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"blog2-768x668.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:668;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"small\";a:4:{s:4:\"file\";s:17:\"blog2-120x104.jpg\";s:5:\"width\";i:120;s:6:\"height\";i:104;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"custom-size\";a:4:{s:4:\"file\";s:17:\"blog2-700x200.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(762, 113, '_thumbnail_id', '114');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-10-01 09:53:01', '2020-10-01 07:53:01', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>\n<!-- /wp:paragraph -->', 'Lorem ipsum', '', 'publish', 'open', 'open', '', 'witaj-swiecie', '', '', '2020-10-09 14:58:54', '2020-10-09 12:58:54', '', 0, 'http://doberman.local.com/?p=1', 0, 'post', '', 1),
(2, 1, '2020-10-01 09:53:01', '2020-10-01 07:53:01', '', 'Przykładowa strona', '', 'publish', 'closed', 'open', '', 'przykladowa-strona', '', '', '2020-10-05 15:44:51', '2020-10-05 13:44:51', '', 0, 'http://doberman.local.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-10-01 09:53:01', '2020-10-01 07:53:01', '<!-- wp:heading --><h2>Kim jesteśmy</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Adres naszej strony internetowej to: http://doberman.local.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Jakie dane osobiste zbieramy i dlaczego je zbieramy</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Komentarze</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Kiedy odwiedzający witrynę zostawia komentarz, zbieramy dane widoczne w formularzu komentowania, jak i adres IP odwiedzającego oraz podpis jego przeglądarki jako pomoc przy wykrywaniu spamu.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Zanonimizowany ciąg znaków stworzony na podstawie twojego adresu email (tak zwany hash) może zostać przesłany do usługi Gravatar w celu sprawdzenia czy jej używasz. Polityka prywatności usługi Gravatar jest dostępna tutaj: https://automattic.com/privacy/. Po zatwierdzeniu komentarza twój obrazek profilowy jest widoczny publicznie w kontekście twojego komentarza.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli jesteś zarejestrowanym użytkownikiem i wgrywasz na witrynę obrazki, powinieneś unikać przesyłania obrazków z tagami EXIF lokalizacji. Odwiedzający stronę mogą pobrać i odczytać pełne dane lokalizacyjne z obrazków w witrynie.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formularze kontaktowe</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Ciasteczka</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli zostawisz na naszej witrynie komentarz, będziesz mógł wybrać opcję zapisu twojej nazwy, adresu email i adresu strony internetowej w ciasteczkach, dzięki którym podczas pisania kolejnych komentarzy powyższe informacje będą już dogodnie uzupełnione. Te ciasteczka wygasają po roku.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Jeśli odwiedzisz stronę logowania, utworzymy tymczasowe ciasteczko na potrzeby sprawdzenia czy twoja przeglądarka akceptuje ciasteczka. To ciasteczko nie zawiera żadnych danych osobistych i zostanie wyrzucone, kiedy zamkniesz przeglądarkę.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Podczas logowania tworzymy dodatkowo kilka ciasteczek potrzebnych do zapisu twoich informacji logowania oraz wybranych opcji ekranu. Ciasteczka logowania wygasają po dwóch dniach, a opcji ekranu po roku. Jeśli zaznaczysz opcję &bdquo;Pamiętaj mnie&rdquo;, logowanie wygaśnie po dwóch tygodniach. Jeśli wylogujesz się ze swojego konta, ciasteczka logowania zostaną usunięte.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Jeśli zmodyfikujesz albo opublikujesz artykuł, w twojej przeglądarce zostanie zapisane dodatkowe ciasteczko. To ciasteczko nie zawiera żadnych danych osobistych, wskazując po prostu na identyfikator przed chwilą edytowanego artykułu. Wygasa ono po 1 dniu.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Osadzone treści z innych witryn</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artykuły na tej witrynie mogą zawierać osadzone treści (np. filmy, obrazki, artykuły itp.). Osadzone treści z innych witryn zachowują się analogicznie do tego, jakby użytkownik odwiedził bezpośrednio konkretną witrynę.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Witryny mogą zbierać informacje o tobie, używać ciasteczek, dołączać dodatkowe, zewnętrzne systemy śledzenia i monitorować twoje interakcje z osadzonym materiałem, włączając w to śledzenie twoich interakcji z osadzonym materiałem jeśli posiadasz konto i jesteś zalogowany w tamtej witrynie.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analiza statystyk</h3><!-- /wp:heading --><!-- wp:heading --><h2>Z kim dzielimy się danymi</h2><!-- /wp:heading --><!-- wp:heading --><h2>Jak długo przechowujemy twoje dane</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli zostawisz komentarz, jego treść i metadane będą przechowywane przez czas nieokreślony. Dzięki temu jesteśmy w stanie rozpoznawać i zatwierdzać kolejne komentarze automatycznie, bez wysyłania ich do każdorazowej moderacji.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Dla użytkowników którzy zarejestrowali się na naszej stronie internetowej (jeśli tacy są), przechowujemy również informacje osobiste wprowadzone w profilu. Każdy użytkownik może dokonać wglądu, korekty albo skasować swoje informacje osobiste w dowolnej chwili (nie licząc nazwy użytkownika, której nie można zmienić). Administratorzy strony również mogą przeglądać i modyfikować te informacje.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Jakie masz prawa do swoich danych</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Jeśli masz konto użytkownika albo dodałeś komentarze w tej witrynie, możesz zażądać dostarczenia pliku z wyeksportowanym kompletem twoich danych osobistych będących w naszym posiadaniu, w tym całość tych dostarczonych przez ciebie. Możesz również zażądać usunięcia przez nas całości twoich danych osobistych w naszym posiadaniu. Nie dotyczy to żadnych danych które jesteśmy zobligowani zachować ze względów administracyjnych, prawnych albo bezpieczeństwa.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Gdzie przesyłamy dane</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Komentarze gości mogą być sprawdzane za pomocą automatycznej usługi wykrywania spamu.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Twoje dane kontaktowe</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informacje dodatkowe</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jak chronimy twoje dane?</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jakie mamy obowiązujące procedury w przypadku naruszenia prywatności danych</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Od jakich stron trzecich otrzymujemy dane</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Jakie automatyczne podejmowanie decyzji i/lub tworzenie profili przeprowadzamy z użyciem danych użytkownika</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Branżowe wymogi regulacyjne dotyczące ujawniania informacji</h3><!-- /wp:heading -->', 'Polityka prywatności', '', 'draft', 'closed', 'open', '', 'polityka-prywatnosci', '', '', '2020-10-01 09:53:01', '2020-10-01 07:53:01', '', 0, 'http://doberman.local.com/?page_id=3', 0, 'page', '', 0),
(5, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'O nas', '', 'publish', 'closed', 'closed', '', 'o-nas', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=5', 1, 'nav_menu_item', '', 0),
(6, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Nasza oferta', '', 'publish', 'closed', 'closed', '', 'nasza-oferta', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=6', 2, 'nav_menu_item', '', 0),
(7, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Psy', '', 'publish', 'closed', 'closed', '', 'psy', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=7', 7, 'nav_menu_item', '', 0),
(8, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=8', 8, 'nav_menu_item', '', 0),
(9, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=9', 9, 'nav_menu_item', '', 0),
(10, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Kontakt', '', 'publish', 'closed', 'closed', '', 'kontakt', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=10', 10, 'nav_menu_item', '', 0),
(11, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'PL / EN / DE', '', 'publish', 'closed', 'closed', '', 'pl-en-de', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=11', 11, 'nav_menu_item', '', 0),
(12, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'O rasie', '', 'publish', 'closed', 'closed', '', 'o-rasie', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=12', 3, 'nav_menu_item', '', 0),
(13, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Szczenięta', '', 'publish', 'closed', 'closed', '', 'szczenieta', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=13', 4, 'nav_menu_item', '', 0),
(14, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Nasze dobermany', '', 'publish', 'closed', 'closed', '', 'nasze-dobermany', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=14', 5, 'nav_menu_item', '', 0),
(15, 1, '2020-10-01 11:03:12', '2020-10-01 09:03:12', '', 'Trening i szkolenia', '', 'publish', 'closed', 'closed', '', 'trening-i-szkolenia', '', '', '2020-10-11 21:52:30', '2020-10-11 19:52:30', '', 0, 'http://doberman.local.com/?p=15', 6, 'nav_menu_item', '', 0),
(16, 1, '2020-10-01 11:06:35', '2020-10-01 09:06:35', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-10-01 09:06:35\"\n    },\n    \"page_on_front\": {\n        \"value\": \"2\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-10-01 09:06:35\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '19cbfdbf-c535-4144-9348-775793607990', '', '', '2020-10-01 11:06:35', '2020-10-01 09:06:35', '', 0, 'http://doberman.local.com/2020/10/01/19cbfdbf-c535-4144-9348-775793607990/', 0, 'customize_changeset', '', 0),
(17, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Basic content', 'basic-content', 'publish', 'closed', 'closed', '', 'group_5f7b16fb1d850', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 0, 'http://doberman.local.com/?post_type=acf-field-group&#038;p=17', 0, 'acf-field-group', '', 0),
(18, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:1:{s:20:\"layout_5f7b1748301d7\";a:6:{s:3:\"key\";s:20:\"layout_5f7b1748301d7\";s:5:\"label\";s:7:\"Content\";s:4:\"name\";s:7:\"content\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:12:\"Dodaj wiersz\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Flexible content', 'flexible_content', 'publish', 'closed', 'closed', '', 'field_5f7b17362508c', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 17, 'http://doberman.local.com/?post_type=acf-field&p=18', 0, 'acf-field', '', 0),
(19, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:10:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:7:\"layouts\";a:3:{s:20:\"layout_5f7b176c905f9\";a:6:{s:3:\"key\";s:20:\"layout_5f7b176c905f9\";s:5:\"label\";s:16:\"Układ 1 kolumny\";s:4:\"name\";s:9:\"columns_1\";s:7:\"display\";s:5:\"table\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_5f7b17d025090\";a:6:{s:3:\"key\";s:20:\"layout_5f7b17d025090\";s:5:\"label\";s:16:\"Układ 2 kolumny\";s:4:\"name\";s:9:\"columns_2\";s:7:\"display\";s:5:\"table\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_5f7b181825091\";a:6:{s:3:\"key\";s:20:\"layout_5f7b181825091\";s:5:\"label\";s:16:\"Układ 3 kolumny\";s:4:\"name\";s:9:\"columns_3\";s:7:\"display\";s:5:\"table\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:12:\"Dodaj wiersz\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Sekcja', 'section', 'publish', 'closed', 'closed', '', 'field_5f7b17562508d', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 18, 'http://doberman.local.com/?post_type=acf-field&p=19', 0, 'acf-field', '', 0),
(20, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b176c905f9\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 1', 'col_1', 'publish', 'closed', 'closed', '', 'field_5f7b177d2508e', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=20', 0, 'acf-field', '', 0),
(21, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 1', 'col_1', 'publish', 'closed', 'closed', '', 'field_5f7b183025092', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=21', 0, 'acf-field', '', 0),
(22, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 2', 'col_2', 'publish', 'closed', 'closed', '', 'field_5f7b184c25093', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=22', 1, 'acf-field', '', 0),
(23, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 1', 'col_1', 'publish', 'closed', 'closed', '', 'field_5f7b187025094', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=23', 0, 'acf-field', '', 0),
(24, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 2', 'col_2', 'publish', 'closed', 'closed', '', 'field_5f7b188525095', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=24', 1, 'acf-field', '', 0),
(25, 1, '2020-10-05 15:00:17', '2020-10-05 13:00:17', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 3', 'col_3', 'publish', 'closed', 'closed', '', 'field_5f7b189525096', '', '', '2020-10-05 15:00:17', '2020-10-05 13:00:17', '', 19, 'http://doberman.local.com/?post_type=acf-field&p=25', 2, 'acf-field', '', 0),
(26, 1, '2020-10-05 15:05:08', '2020-10-05 13:05:08', '', 'Przykładowa strona', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-10-05 15:05:08', '2020-10-05 13:05:08', '', 2, 'http://doberman.local.com/2020/10/05/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Black banner + image', 'black-banner-image', 'trash', 'closed', 'closed', '', 'group_5f7b2055a0e6c__trashed', '', '', '2020-10-05 16:08:44', '2020-10-05 14:08:44', '', 0, 'http://doberman.local.com/?p=27', 0, 'acf-field-group', '', 0),
(28, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:1:{s:20:\"layout_5f7b1748301d7\";a:6:{s:3:\"key\";s:20:\"layout_5f7b1748301d7\";s:5:\"label\";s:7:\"Content\";s:4:\"name\";s:7:\"content\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:12:\"Dodaj wiersz\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'blackbanner_image', 'blackbanner_image', 'trash', 'closed', 'closed', '', 'field_5f7b2055b6a2f__trashed', '', '', '2020-10-05 16:08:44', '2020-10-05 14:08:44', '', 27, 'http://doberman.local.com/?post_type=acf-field&#038;p=28', 0, 'acf-field', '', 0),
(29, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Obrazek po lewej', 'image_left', 'publish', 'closed', 'closed', '', 'field_5f7b2055ba7ea', '', '', '2020-10-05 15:52:49', '2020-10-05 13:52:49', '', 28, 'http://doberman.local.com/?post_type=acf-field&#038;p=29', 0, 'acf-field', '', 0),
(33, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Kolumna 1', 'col_1', 'publish', 'closed', 'closed', '', 'field_5f7b2055c9106', '', '', '2020-10-05 15:48:59', '2020-10-05 13:48:59', '', 29, 'http://doberman.local.com/?post_type=acf-field&#038;p=33', 0, 'acf-field', '', 0),
(34, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Kolumna 2', 'col_2', 'publish', 'closed', 'closed', '', 'field_5f7b2055ccd25', '', '', '2020-10-05 15:32:05', '2020-10-05 13:32:05', '', 29, 'http://doberman.local.com/?post_type=acf-field&p=34', 1, 'acf-field', '', 0),
(35, 1, '2020-10-05 15:32:05', '2020-10-05 13:32:05', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b181825091\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Kolumna 3', 'col_3', 'publish', 'closed', 'closed', '', 'field_5f7b2055d0771', '', '', '2020-10-05 15:48:59', '2020-10-05 13:48:59', '', 29, 'http://doberman.local.com/?post_type=acf-field&#038;p=35', 2, 'acf-field', '', 0),
(36, 1, '2020-10-05 15:34:16', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-10-05 15:34:16', '0000-00-00 00:00:00', '', 0, 'http://doberman.local.com/?page_id=36', 0, 'page', '', 0),
(37, 1, '2020-10-05 15:39:15', '2020-10-05 13:39:15', '', 'Doberman', '', 'inherit', 'open', 'closed', '', 'doberman', '', '', '2020-10-05 15:39:15', '2020-10-05 13:39:15', '', 2, 'http://doberman.local.com/wp-content/uploads/2020/10/Doberman.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2020-10-05 15:41:32', '2020-10-05 13:41:32', '', 'Warstwa 18', '', 'inherit', 'open', 'closed', '', 'warstwa-18', '', '', '2020-10-05 15:41:32', '2020-10-05 13:41:32', '', 2, 'http://doberman.local.com/wp-content/uploads/2020/10/Warstwa-18.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2020-10-05 15:41:41', '2020-10-05 13:41:41', '', 'Przykładowa strona', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-10-05 15:41:41', '2020-10-05 13:41:41', '', 2, 'http://doberman.local.com/2020/10/05/2-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2020-10-05 15:44:51', '2020-10-05 13:44:51', '', 'Przykładowa strona', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-10-05 15:44:51', '2020-10-05 13:44:51', '', 2, 'http://doberman.local.com/2020/10/05/2-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2020-10-05 15:52:49', '2020-10-05 13:52:49', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nagłówek', 'header_text', 'publish', 'closed', 'closed', '', 'field_5f7b249d05745', '', '', '2020-10-05 15:52:49', '2020-10-05 13:52:49', '', 28, 'http://doberman.local.com/?post_type=acf-field&p=41', 1, 'acf-field', '', 0),
(42, 1, '2020-10-05 15:52:49', '2020-10-05 13:52:49', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Opis', 'description_text', 'publish', 'closed', 'closed', '', 'field_5f7b24b005746', '', '', '2020-10-05 15:52:49', '2020-10-05 13:52:49', '', 28, 'http://doberman.local.com/?post_type=acf-field&p=42', 2, 'acf-field', '', 0),
(43, 1, '2020-10-05 15:52:49', '2020-10-05 13:52:49', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Banner', 'banner_text', 'publish', 'closed', 'closed', '', 'field_5f7b24e005747', '', '', '2020-10-05 15:52:49', '2020-10-05 13:52:49', '', 28, 'http://doberman.local.com/?post_type=acf-field&p=43', 3, 'acf-field', '', 0),
(44, 1, '2020-10-05 15:52:49', '2020-10-05 13:52:49', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Obrazek po prawej (większy)', 'image_right', 'publish', 'closed', 'closed', '', 'field_5f7b251505748', '', '', '2020-10-05 15:52:49', '2020-10-05 13:52:49', '', 28, 'http://doberman.local.com/?post_type=acf-field&p=44', 4, 'acf-field', '', 0),
(45, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Banner + image', 'banner-image', 'trash', 'closed', 'closed', '', 'group_5f7b287584ab8__trashed', '', '', '2020-10-09 10:04:03', '2020-10-09 08:04:03', '', 0, 'http://doberman.local.com/?post_type=acf-field-group&#038;p=45', 0, 'acf-field-group', '', 0),
(46, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image left', 'image_left', 'trash', 'closed', 'closed', '', 'field_5f7b2882aaebf__trashed', '', '', '2020-10-09 10:04:03', '2020-10-09 08:04:03', '', 45, 'http://doberman.local.com/?post_type=acf-field&#038;p=46', 0, 'acf-field', '', 0),
(47, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titile_text', 'titile_text', 'trash', 'closed', 'closed', '', 'field_5f7b2897aaec0__trashed', '', '', '2020-10-09 10:04:04', '2020-10-09 08:04:04', '', 45, 'http://doberman.local.com/?post_type=acf-field&#038;p=47', 1, 'acf-field', '', 0),
(48, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Descritpion_text', 'descritpion_text', 'trash', 'closed', 'closed', '', 'field_5f7b28aaaaec1__trashed', '', '', '2020-10-09 10:04:04', '2020-10-09 08:04:04', '', 45, 'http://doberman.local.com/?post_type=acf-field&#038;p=48', 2, 'acf-field', '', 0),
(49, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Button_text', 'button_text', 'trash', 'closed', 'closed', '', 'field_5f7b28b7aaec2__trashed', '', '', '2020-10-09 10:04:04', '2020-10-09 08:04:04', '', 45, 'http://doberman.local.com/?post_type=acf-field&#038;p=49', 3, 'acf-field', '', 0),
(50, 1, '2020-10-05 16:08:28', '2020-10-05 14:08:28', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Right_image', 'right_image', 'trash', 'closed', 'closed', '', 'field_5f7b28c7aaec3__trashed', '', '', '2020-10-09 10:04:04', '2020-10-09 08:04:04', '', 45, 'http://doberman.local.com/?post_type=acf-field&#038;p=50', 4, 'acf-field', '', 0),
(51, 1, '2020-10-05 16:18:32', '2020-10-05 14:18:32', '', 'Strona główna', '', 'publish', 'closed', 'closed', '', 'strona-glowna', '', '', '2020-10-09 14:30:45', '2020-10-09 12:30:45', '', 0, 'http://doberman.local.com/?page_id=51', 0, 'page', '', 0),
(53, 1, '2020-10-05 16:18:32', '2020-10-05 14:18:32', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-05 16:18:32', '2020-10-05 14:18:32', '', 51, 'http://doberman.local.com/2020/10/05/51-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-10-05 16:18:33', '2020-10-05 14:18:33', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-05 16:18:33', '2020-10-05 14:18:33', '', 51, 'http://doberman.local.com/2020/10/05/51-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-10-07 10:03:39', '2020-10-07 08:03:39', '', 'Imię #1', '', 'publish', 'closed', 'closed', '', 'imie-1', '', '', '2020-10-07 10:03:39', '2020-10-07 08:03:39', '', 0, 'http://doberman.local.com/?post_type=puppies-blank&#038;p=55', 0, 'puppies-blank', '', 0),
(56, 1, '2020-10-07 10:03:23', '2020-10-07 08:03:23', '', 'pup1', '', 'inherit', 'open', 'closed', '', 'pup1', '', '', '2020-10-07 10:03:23', '2020-10-07 08:03:23', '', 55, 'http://doberman.local.com/wp-content/uploads/2020/10/pup1.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2020-10-07 10:04:13', '2020-10-07 08:04:13', '', 'Imię #2', '', 'publish', 'closed', 'closed', '', 'imie-2', '', '', '2020-10-07 10:04:13', '2020-10-07 08:04:13', '', 0, 'http://doberman.local.com/?post_type=puppies-blank&#038;p=57', 0, 'puppies-blank', '', 0),
(58, 1, '2020-10-07 10:04:04', '2020-10-07 08:04:04', '', 'pup2', '', 'inherit', 'open', 'closed', '', 'pup2', '', '', '2020-10-07 10:04:04', '2020-10-07 08:04:04', '', 57, 'http://doberman.local.com/wp-content/uploads/2020/10/pup2.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2020-10-07 10:16:21', '2020-10-07 08:16:21', '', 'Imię #3', '', 'publish', 'closed', 'closed', '', 'imie-3', '', '', '2020-10-07 10:16:21', '2020-10-07 08:16:21', '', 0, 'http://doberman.local.com/?post_type=puppies-blank&#038;p=59', 0, 'puppies-blank', '', 0),
(60, 1, '2020-10-07 10:04:31', '2020-10-07 08:04:31', '', 'pup3', '', 'inherit', 'open', 'closed', '', 'pup3', '', '', '2020-10-07 10:04:31', '2020-10-07 08:04:31', '', 59, 'http://doberman.local.com/wp-content/uploads/2020/10/pup3.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2020-10-07 10:16:44', '2020-10-07 08:16:44', '', 'Imię #4', '', 'publish', 'closed', 'closed', '', 'imie-4', '', '', '2020-10-07 10:16:44', '2020-10-07 08:16:44', '', 0, 'http://doberman.local.com/?post_type=puppies-blank&#038;p=61', 0, 'puppies-blank', '', 0),
(62, 1, '2020-10-07 10:16:40', '2020-10-07 08:16:40', '', 'pup4', '', 'inherit', 'open', 'closed', '', 'pup4', '', '', '2020-10-07 10:16:40', '2020-10-07 08:16:40', '', 61, 'http://doberman.local.com/wp-content/uploads/2020/10/pup4.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2020-10-07 13:10:42', '2020-10-07 11:10:42', '', 'Imię 1', '', 'publish', 'closed', 'closed', '', 'imie-1', '', '', '2020-10-07 14:02:10', '2020-10-07 12:02:10', '', 0, 'http://doberman.local.com/?post_type=puppies&#038;p=63', 1, 'puppies', '', 0),
(64, 1, '2020-10-07 13:10:25', '2020-10-07 11:10:25', '', 'pup1', '', 'inherit', 'open', 'closed', '', 'pup1-2', '', '', '2020-10-07 13:10:25', '2020-10-07 11:10:25', '', 63, 'http://doberman.local.com/wp-content/uploads/2020/10/pup1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2020-10-07 13:16:26', '2020-10-07 11:16:26', '', 'Imię 2', '', 'publish', 'closed', 'closed', '', 'imie-2', '', '', '2020-10-07 14:06:07', '2020-10-07 12:06:07', '', 0, 'http://doberman.local.com/?post_type=puppies&#038;p=65', 2, 'puppies', '', 0),
(66, 1, '2020-10-07 13:17:29', '2020-10-07 11:17:29', '', 'Imię 3', '', 'publish', 'closed', 'closed', '', 'imie-3', '', '', '2020-10-07 14:06:16', '2020-10-07 12:06:16', '', 0, 'http://doberman.local.com/?post_type=puppies&#038;p=66', 3, 'puppies', '', 0),
(67, 1, '2020-10-07 13:17:51', '2020-10-07 11:17:51', '', 'Imię 4', '', 'publish', 'closed', 'closed', '', 'imie-4', '', '', '2020-10-07 14:06:24', '2020-10-07 12:06:24', '', 0, 'http://doberman.local.com/?post_type=puppies&#038;p=67', 4, 'puppies', '', 0),
(68, 1, '2020-10-07 14:01:34', '2020-10-07 12:01:34', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"puppies\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:1:{i:0;s:11:\"the_content\";}s:11:\"description\";s:0:\"\";}', 'Puppies', 'puppies', 'publish', 'closed', 'closed', '', 'group_5f7dadf3ac5df', '', '', '2020-10-07 14:01:34', '2020-10-07 12:01:34', '', 0, 'http://doberman.local.com/?post_type=acf-field-group&#038;p=68', 0, 'acf-field-group', '', 0),
(69, 1, '2020-10-07 14:01:34', '2020-10-07 12:01:34', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'puppies_descr', 'puppies_descr', 'publish', 'closed', 'closed', '', 'field_5f7dadfd56d5f', '', '', '2020-10-07 14:01:34', '2020-10-07 12:01:34', '', 68, 'http://doberman.local.com/?post_type=acf-field&p=69', 0, 'acf-field', '', 0),
(70, 1, '2020-10-07 14:30:46', '2020-10-07 12:30:46', '', 'Imię 5', '', 'publish', 'closed', 'closed', '', 'imie-5', '', '', '2020-10-07 14:30:46', '2020-10-07 12:30:46', '', 0, 'http://doberman.local.com/?post_type=puppies&#038;p=70', 5, 'puppies', '', 0),
(71, 1, '2020-10-07 14:30:43', '2020-10-07 12:30:43', '', 'pup5', '', 'inherit', 'open', 'closed', '', 'pup5', '', '', '2020-10-07 14:30:43', '2020-10-07 12:30:43', '', 70, 'http://doberman.local.com/wp-content/uploads/2020/10/pup5.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2020-10-08 13:03:13', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-10-08 13:03:13', '0000-00-00 00:00:00', '', 0, 'http://doberman.local.com/?p=72', 0, 'post', '', 0),
(75, 1, '2020-10-09 09:58:37', '2020-10-09 07:58:37', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Components', 'components', 'publish', 'closed', 'closed', '', 'group_5f80182d9e446', '', '', '2020-10-09 13:03:15', '2020-10-09 11:03:15', '', 0, 'http://doberman.local.com/?p=75', 0, 'acf-field-group', '', 0),
(76, 1, '2020-10-09 09:58:37', '2020-10-09 07:58:37', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:1:{s:20:\"layout_5f7b1748301d7\";a:6:{s:3:\"key\";s:20:\"layout_5f7b1748301d7\";s:5:\"label\";s:7:\"Content\";s:4:\"name\";s:7:\"content\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:12:\"Dodaj wiersz\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Components', 'components', 'publish', 'closed', 'closed', '', 'field_5f80182db7019', '', '', '2020-10-09 10:02:53', '2020-10-09 08:02:53', '', 75, 'http://doberman.local.com/?post_type=acf-field&#038;p=76', 0, 'acf-field', '', 0),
(77, 1, '2020-10-09 09:58:37', '2020-10-09 07:58:37', 'a:10:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b1748301d7\";s:7:\"layouts\";a:2:{s:20:\"layout_5f7b176c905f9\";a:6:{s:3:\"key\";s:20:\"layout_5f7b176c905f9\";s:5:\"label\";s:22:\"Grafika - cała strona\";s:4:\"name\";s:11:\"component_1\";s:7:\"display\";s:5:\"table\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_5f7b17d025090\";a:6:{s:3:\"key\";s:20:\"layout_5f7b17d025090\";s:5:\"label\";s:16:\"Button + grafika\";s:4:\"name\";s:11:\"component_2\";s:7:\"display\";s:5:\"table\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:12:\"Dodaj wiersz\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Sekcja', 'section', 'publish', 'closed', 'closed', '', 'field_5f80182dbac72', '', '', '2020-10-09 10:02:53', '2020-10-09 08:02:53', '', 76, 'http://doberman.local.com/?post_type=acf-field&#038;p=77', 0, 'acf-field', '', 0),
(84, 1, '2020-10-09 10:02:53', '2020-10-09 08:02:53', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b176c905f9\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tytuł', 'title', 'publish', 'closed', 'closed', '', 'field_5f8018c8efe22', '', '', '2020-10-09 12:52:24', '2020-10-09 10:52:24', '', 77, 'http://doberman.local.com/?post_type=acf-field&#038;p=84', 0, 'acf-field', '', 0),
(85, 1, '2020-10-09 10:02:53', '2020-10-09 08:02:53', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b176c905f9\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Opis', 'description', 'publish', 'closed', 'closed', '', 'field_5f8018dfefe23', '', '', '2020-10-09 10:02:53', '2020-10-09 08:02:53', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=85', 1, 'acf-field', '', 0),
(86, 1, '2020-10-09 10:02:53', '2020-10-09 08:02:53', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b176c905f9\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tekst button', 'button_text', 'publish', 'closed', 'closed', '', 'field_5f8018f0efe24', '', '', '2020-10-09 10:02:53', '2020-10-09 08:02:53', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=86', 2, 'acf-field', '', 0),
(87, 1, '2020-10-09 10:02:53', '2020-10-09 08:02:53', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b176c905f9\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Tło', 'background', 'publish', 'closed', 'closed', '', 'field_5f80190cefe25', '', '', '2020-10-09 10:02:53', '2020-10-09 08:02:53', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=87', 3, 'acf-field', '', 0),
(88, 1, '2020-10-09 10:05:19', '2020-10-09 08:05:19', '', 'dob03', '', 'inherit', 'open', 'closed', '', 'dob03', '', '', '2020-10-09 10:05:19', '2020-10-09 08:05:19', '', 51, 'http://doberman.local.com/wp-content/uploads/2020/10/dob03.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2020-10-09 10:09:25', '2020-10-09 08:09:25', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 10:09:25', '2020-10-09 08:09:25', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2020-10-09 10:14:11', '2020-10-09 08:14:11', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 10:14:11', '2020-10-09 08:14:11', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2020-10-09 10:16:44', '2020-10-09 08:16:44', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 10:16:44', '2020-10-09 08:16:44', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2020-10-09 10:43:50', '2020-10-09 08:43:50', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 10:43:50', '2020-10-09 08:43:50', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2020-10-09 11:10:05', '2020-10-09 09:10:05', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 11:10:05', '2020-10-09 09:10:05', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2020-10-09 11:33:36', '0000-00-00 00:00:00', '', 'Automatycznie zapisany szkic', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-10-09 11:33:36', '0000-00-00 00:00:00', '', 0, 'http://doberman.local.com/?post_type=acf-field-group&p=94', 0, 'acf-field-group', '', 0),
(95, 1, '2020-10-09 11:45:58', '2020-10-09 09:45:58', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 11:45:58', '2020-10-09 09:45:58', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2020-10-09 12:11:49', '2020-10-09 10:11:49', '{\n    \"page_on_front\": {\n        \"value\": \"51\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-10-09 10:11:49\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '44ed54bf-9def-4f18-bc43-c42dcf0433e6', '', '', '2020-10-09 12:11:49', '2020-10-09 10:11:49', '', 0, 'http://doberman.local.com/2020/10/09/44ed54bf-9def-4f18-bc43-c42dcf0433e6/', 0, 'customize_changeset', '', 0),
(97, 1, '2020-10-09 12:47:33', '2020-10-09 10:47:33', '', 'dob04', '', 'inherit', 'open', 'closed', '', 'dob04', '', '', '2020-10-09 12:47:33', '2020-10-09 10:47:33', '', 51, 'http://doberman.local.com/wp-content/uploads/2020/10/dob04.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2020-10-09 12:47:54', '2020-10-09 10:47:54', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 12:47:54', '2020-10-09 10:47:54', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2020-10-09 12:48:09', '2020-10-09 10:48:09', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 12:48:09', '2020-10-09 10:48:09', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2020-10-09 12:52:24', '2020-10-09 10:52:24', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tytuł', 'title', 'publish', 'closed', 'closed', '', 'field_5f80407dfac8c', '', '', '2020-10-09 12:52:24', '2020-10-09 10:52:24', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=100', 0, 'acf-field', '', 0),
(101, 1, '2020-10-09 12:52:24', '2020-10-09 10:52:24', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Opis', 'description', 'publish', 'closed', 'closed', '', 'field_5f804095fac8d', '', '', '2020-10-09 12:52:24', '2020-10-09 10:52:24', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=101', 1, 'acf-field', '', 0),
(102, 1, '2020-10-09 12:52:24', '2020-10-09 10:52:24', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tekst button', 'button_text', 'publish', 'closed', 'closed', '', 'field_5f8040a3fac8e', '', '', '2020-10-09 12:52:24', '2020-10-09 10:52:24', '', 77, 'http://doberman.local.com/?post_type=acf-field&p=102', 2, 'acf-field', '', 0),
(104, 1, '2020-10-09 12:52:24', '2020-10-09 10:52:24', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5f7b17d025090\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Obrazek z prawej', 'img_right', 'publish', 'closed', 'closed', '', 'field_5f8040cefac90', '', '', '2020-10-09 13:03:14', '2020-10-09 11:03:14', '', 77, 'http://doberman.local.com/?post_type=acf-field&#038;p=104', 3, 'acf-field', '', 0),
(105, 1, '2020-10-09 13:05:53', '2020-10-09 11:05:53', '', 'dob01', '', 'inherit', 'open', 'closed', '', 'dob01', '', '', '2020-10-09 13:05:53', '2020-10-09 11:05:53', '', 51, 'http://doberman.local.com/wp-content/uploads/2020/10/dob01.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2020-10-09 13:06:32', '2020-10-09 11:06:32', '', 'dob02', '', 'inherit', 'open', 'closed', '', 'dob02', '', '', '2020-10-09 13:06:32', '2020-10-09 11:06:32', '', 51, 'http://doberman.local.com/wp-content/uploads/2020/10/dob02.jpg', 0, 'attachment', 'image/jpeg', 0),
(107, 1, '2020-10-09 13:09:18', '2020-10-09 11:09:18', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 13:09:18', '2020-10-09 11:09:18', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(108, 1, '2020-10-09 14:15:11', '2020-10-09 12:15:11', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 14:15:11', '2020-10-09 12:15:11', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2020-10-09 14:30:45', '2020-10-09 12:30:45', '', 'Strona główna', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 14:30:45', '2020-10-09 12:30:45', '', 51, 'http://doberman.local.com/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2020-10-09 14:56:13', '2020-10-09 12:56:13', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>\n<!-- /wp:paragraph -->', 'Lorem ipsum', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2020-10-09 14:56:13', '2020-10-09 12:56:13', '', 1, 'http://doberman.local.com/2020/10/09/1-autosave-v1/', 0, 'revision', '', 0),
(111, 1, '2020-10-09 14:58:20', '2020-10-09 12:58:20', '', 'blog1', '', 'inherit', 'open', 'closed', '', 'blog1', '', '', '2020-10-09 14:58:20', '2020-10-09 12:58:20', '', 1, 'http://doberman.local.com/wp-content/uploads/2020/10/blog1.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2020-10-09 14:58:54', '2020-10-09 12:58:54', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>\n<!-- /wp:paragraph -->', 'Lorem ipsum', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-10-09 14:58:54', '2020-10-09 12:58:54', '', 1, 'http://doberman.local.com/2020/10/09/1-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2020-10-09 14:59:40', '2020-10-09 12:59:40', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>\n<!-- /wp:paragraph -->', 'Lorem ipsum', '', 'publish', 'open', 'open', '', 'lorem-ipsum', '', '', '2020-10-09 14:59:40', '2020-10-09 12:59:40', '', 0, 'http://doberman.local.com/?p=113', 0, 'post', '', 0),
(114, 1, '2020-10-09 14:59:28', '2020-10-09 12:59:28', '', 'blog2', '', 'inherit', 'open', 'closed', '', 'blog2', '', '', '2020-10-09 14:59:28', '2020-10-09 12:59:28', '', 113, 'http://doberman.local.com/wp-content/uploads/2020/10/blog2.jpg', 0, 'attachment', 'image/jpeg', 0),
(115, 1, '2020-10-09 14:59:40', '2020-10-09 12:59:40', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>\n<!-- /wp:paragraph -->', 'Lorem ipsum', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2020-10-09 14:59:40', '2020-10-09 12:59:40', '', 113, 'http://doberman.local.com/2020/10/09/113-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  `term_order` int(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`, `term_order`) VALUES
(1, 'Bez kategorii', 'bez-kategorii', 0, 0),
(2, 'doberman-menu', 'doberman-menu', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(5, 2, 0),
(6, 2, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(27, 1, 0),
(75, 1, 0),
(113, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'nav_menu', '', 0, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'prime-user'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'plugin_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"3da9e69ee426d92bb23a9559a61d27ffc85540d875c30fd410112ffdcf1d818f\";a:4:{s:10:\"expiration\";i:1602617640;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36\";s:5:\"login\";i:1602444840;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '72'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:25:\"add-post-type-html5-blank\";i:1;s:12:\"add-post_tag\";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'wp_user-settings', 'libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1601905296'),
(24, 1, 'closedpostboxes_page', 'a:0:{}'),
(25, 1, 'metaboxhidden_page', 'a:0:{}'),
(26, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(27, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(28, 1, 'meta-box-order_acf-field-group', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:54:\"slugdiv,acf-field-group-fields,acf-field-group-options\";s:8:\"advanced\";s:25:\"acf-field-group-locations\";}'),
(29, 1, 'screen_layout_acf-field-group', '2'),
(30, 1, 'meta-box-order_page', 'a:4:{s:6:\"normal\";s:71:\"acf-group_5f7b287584ab8,acf-group_5f80182d9e446,acf-group_5f7b16fb1d850\";s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:0:\"\";s:8:\"advanced\";s:0:\"\";}');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'prime-user', '$P$B32LzFBsZbt2O4DnantLASAzQcQGqq0', 'prime-user', 'prime.dev.work@gmail.com', 'http://doberman.local.com', '2020-10-01 07:53:01', '', 0, 'prime-user');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indeksy dla tabeli `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indeksy dla tabeli `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indeksy dla tabeli `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indeksy dla tabeli `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indeksy dla tabeli `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indeksy dla tabeli `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indeksy dla tabeli `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indeksy dla tabeli `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indeksy dla tabeli `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indeksy dla tabeli `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indeksy dla tabeli `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=378;

--
-- AUTO_INCREMENT dla tabeli `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=763;

--
-- AUTO_INCREMENT dla tabeli `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT dla tabeli `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT dla tabeli `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
