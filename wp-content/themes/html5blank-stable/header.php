<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-184333040-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-184333040-1');
		</script>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/doggo_icon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=BenchNine&family=Kaushan+Script&display=swap" rel="stylesheet">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        // conditionizr.config({
        //     assets: '<?php echo get_template_directory_uri(); ?>',
        //     tests: {}
        // });
        </script>

	</head>
	<body <?php body_class(); ?>>
	<div id="preloader">
		<img src="<?php echo get_template_directory_uri(); ?>/img/doggo_logo.png" alt="preloader">
	</div>

		<!-- wrapper -->
		<!-- <div class="wrapper"> -->

			<!-- header -->
		<header id="main" class="header clear container-fluid" role="banner">

			
				<div class="row">
					<div class="col-12">
					<div class="container">
						<div class="row">
							<div class="col-12">
									<div class="logo">
										<a href="<?php echo home_url(); ?>">
											<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/doggo_logo.jpg" width="251px" height="243px" alt="Logo" >
										</a>
									</div>

											<button class="hamburger hamburger--vortex-r menu-expand-btn" type="button">
												<span class="hamburger-box">
													<span class="hamburger-inner"></span>
												</span>
											</button>
					<!-- nav -->
											<div class="main-menu">
												<?php
												echo "<nav id='doberman-menu'>";
													wp_nav_menu( array(
													'theme_location' => 'doberman-menu',
													'container_class' => 'custom-menu-class',
													'after' => '<span class="arrow"></span>' ) );
												echo "</nav>";
												?>
											</div>
					<!-- /nav -->
							</div>
						</div>
					</div>
				</div>
			</div>
							
							
		</header>
			<div class="header-wrapper"></div>
			<!-- /header -->
