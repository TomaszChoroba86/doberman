<div class="components" id="components">
	<?php
		if( have_rows('components') ):
			while ( have_rows('components') ) : the_row();
				if( get_row_layout() == 'component_1' ):
					$title = get_sub_field('title');
					$description = get_sub_field('description');
					$button_text = get_sub_field('button_text');
					$background = get_sub_field('background');
					$url = get_sub_field('url');?>
								
							<section class="component_1">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="col-12">
												<div class="component_1-image">
													<div class="shadow"></div>
														<img 
															class='lazy thumbnail' 
															data-src="<?php echo $background['sizes']['large']; ?>" 
															alt="<?php echo $background['title'];?>" 
															width="<?php echo $background['width'];?>" 
															height="<?php echo $background['height'];?>" 
															data-srcset="
															<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
															<?php echo $background['url'].' '.$background['width'].'w'; ?>,
															<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
															<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >
													
												</div>
												
													<div class="link-box">
														<a href="<?php echo get_home_url ();?><?php echo $url ?>" class="component_1-title"><?php echo $title ?></a>
														<p class="component_1-text"><?php echo $description ?></p>
														<span class="component_1-button"><?php echo $button_text ?></span>
													</div>

												
											</div>
										</div>
									</div>
								</div>
							</section>
										
					<?php
					elseif( get_row_layout() == 'component_2' ):
						$title = get_sub_field('title');
						$description = add_nbsp(get_sub_field('description'));
						$button_text = get_sub_field('button_text');
						$background = get_sub_field('img_right');
						$background_mobile = get_sub_field('img_right_mobile');
						$url = get_sub_field('url');?>
				
							<section class="component_2 wow animate__fadeInUp" data-wow-duration="1s">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="col-lg-5 col-sm-12 col-md-12 comp2-left-area order-last order-xl-first">
												
												<div class="link-box">
														<a href="<?php echo get_home_url ();?><?php echo $url ?>" class="component_2-title"><?php echo $title ?></a>
														<p class="component_2-text"><?php echo $description ?></p>
														<span class="component_2-button"><?php echo $button_text ?></span>
												</div>
											</div>

											<div class="col-lg-7 col-sm-12 col-md-12 comp2-right-area order-sm-first order-md-first">

													<div class="component_2-image">
														<img 
														class='lazy' 
														data-src="<?php echo $background['sizes']['large']; ?>" 
														alt="<?php echo $background['title'];?>" 
														width="<?php echo $background['width'];?>" 
														height="<?php echo $background['height'];?>" 
														data-srcset="
														<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
														<?php echo $background['url'].' '.$background['width'].'w'; ?>,
														<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
														<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >
													</div>
													<div class="component_2-image_mobile">
														<img 
														class='lazy' 
														data-src="<?php echo $background_mobile['sizes']['large']; ?>" 
														alt="<?php echo $background_mobile['title'];?>" 
														width="<?php echo $background_mobile['width'];?>" 
														height="<?php echo $background_mobile['height'];?>" 
														data-srcset="
														<?php echo $background_mobile['sizes']['large'].' '.$background_mobile['sizes']['large-width'].'w'; ?>, 
														<?php echo $background_mobile['url'].' '.$background_mobile['width'].'w'; ?>,
														<?php echo $background_mobile['sizes']['medium'].' '.$background_mobile['sizes']['medium-width'].'w';?>, 
														<?php echo $background_mobile['sizes']['small'].' '.$background_mobile['sizes']['small-width'].'w'; ?>" >
													</div>
											</div>
										</div>
									</div>
								</div>
							</section>


					<?php

					elseif( get_row_layout() == 'section-title' ):
						$title = get_sub_field('title');
						$id = get_sub_field('id');
						?>

							<section class="section-title" id="<?php echo $id ?>">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="puppies-title-box">
												<h1 class="col-12 puppies-title"><?php echo $title?></h1>
											</div>
										</div>
									</div>
								</div>
							</section>

					<?php
					elseif( get_row_layout() == 'video_banner' ):
						$video_file = get_sub_field('video_file');
						$title = add_nbsp(get_sub_field('title'));
						$description = add_nbsp(get_sub_field('description'));
						$button_text = get_sub_field('button_text');
						$url = get_sub_field('url');?>
				
							<section id="banner">
								<div class="container-fluid">
									<div class="container">

										<video autoplay muted loop playsinline id="doberman-video">
											<source src=<?php echo $video_file?> type="video/mp4">
										</video>
											
										<div class="row info">
											<div class="col-lg-12 col-sm-12 link-box">
												<a href="<?php echo get_home_url ();?><?php echo $url ?>" class="title"><?php echo $title ?></a>
												<p class="desription"><?php echo $description ?></p>
												<span class="banner"><?php echo $button_text ?></span>

											</div>
										</div>
										<div class="row arrow">
											<div class="col-lg-6 col-sm-12 link-box">	
											
											<a href="<?php echo get_home_url ();?>#puppies" class="ca3-scroll-down-link ca3-scroll-down-arrow" data-ca3_iconfont="ETmodules" data-ca3_icon="">

											</a>
											
											</div>
										</div>

										
									</div>
								</div>
							</section>

					<?php
					elseif( get_row_layout() == 'template_puppies' ):
						$template_title = get_sub_field('template_title');
						$template = get_sub_field('template');?>

							<?php include( locate_template( $template , false, false )); ?>

							<?php
					elseif( get_row_layout() == 'template' ):
						$template = get_sub_field('template');?>

							<?php include( locate_template( $template , false, false )); ?>

					<?php
					elseif( get_row_layout() == 'img-banner' ):
					$title = get_sub_field('title');
					$background = get_sub_field('background');
					$mobile_background = get_sub_field('mobile_background');
					?>

						<section class="img-banner">
							<div class="container-fluid">
								<div class="container">
									<div class="row desktop">
										<?php if( !empty( $background ) ): ?>
											<div class="col-12 img-banner-background">
											<img src="<?php echo get_template_directory_uri(); ?>/img/pix.png" width="1920px" height="715px" alt="" class="pix">
											<img 
												class='lazy' 
												data-src="<?php echo $background['sizes']['large']; ?>" 
												alt="<?php echo $background['title'];?>" 
												width="<?php echo $background['width'];?>" 
												height="<?php echo $background['height'];?>" 
												data-srcset="
												<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
												<?php echo $background['url'].' '.$background['width'].'w'; ?>,
												<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
												<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >


												
												<h3 class="title"><?php echo $title ?></h3>
												
											</div>
											
										<?php endif; ?>
									</div>

									<div class="row mobile">
										<?php if( !empty( $mobile_background ) ): ?>
											<div class="col-12 img-banner-background">
											<img src="<?php echo get_template_directory_uri(); ?>/img/pix.png" width="1920px" height="715px" alt="" class="pix">
											<img 
												class='lazy' 
												data-src="<?php echo $mobile_background['sizes']['large']; ?>" 
												alt="<?php echo $mobile_background['title'];?>" 
												width="<?php echo $mobile_background['width'];?>" 
												height="<?php echo $mobile_background['height'];?>" 
												data-srcset="
												<?php echo $mobile_background['sizes']['large'].' '.$mobile_background['sizes']['large-width'].'w'; ?>, 
												<?php echo $mobile_background['url'].' '.$mobile_background['width'].'w'; ?>,
												<?php echo $mobile_background['sizes']['medium'].' '.$mobile_background['sizes']['medium-width'].'w';?>, 
												<?php echo $mobile_background['sizes']['small'].' '.$mobile_background['sizes']['small-width'].'w'; ?>" >


												
												<h3 class="title"><?php echo $title ?></h3>
												
											</div>
											
										<?php endif; ?>
									</div>

									<div class="row arrow">
										<div class="col-lg-6 col-sm-12 link-box">	
											<a href="<?php echo get_home_url ();?>#scrolldown" class="ca3-scroll-down-link ca3-scroll-down-arrow" data-ca3_iconfont="ETmodules" data-ca3_icon=""></a>
										</div>
									</div>
										<hr id="scrolldown">
								</div>
							</div>
						</section>		



						<?php
					elseif( get_row_layout() == 'hotel' ):
						$img = get_sub_field('img_left');
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$opensoon = get_sub_field('opensoon');
						$opensoon_text = get_sub_field('opensoon_text');?>

							<section class="hotel wow animate__fadeInUp" data-wow-duration="1s" id="hotel">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<?php if( !empty( $opensoon ) ): ?>
											<div class="open_soon">
												<img class='lazy' 
												data-src="<?php echo $opensoon['sizes']['large']; ?>" 
												alt="<?php echo $opensoon['title'];?>" 
												width="<?php echo $opensoon['width'];?>" 
												height="<?php echo $opensoon['height'];?>" 
												data-srcset="
												<?php echo $opensoon['sizes']['large'].' '.$opensoon['sizes']['large-width'].'w'; ?>, 
												<?php echo $opensoon['url'].' '.$opensoon['width'].'w'; ?>,
												<?php echo $opensoon['sizes']['medium'].' '.$opensoon['sizes']['medium-width'].'w';?>, 
												<?php echo $opensoon['sizes']['small'].' '.$opensoon['sizes']['small-width'].'w'; ?>" >

												<p class="opensoon_text"><?php echo $opensoon_text ?></p>
										</div>
											<?php endif; ?>
										<?php if( !empty( $img ) ): ?>
											<div class="col-lg-6 col-md-12 img_left">
											<img 
												class='lazy' 
												data-src="<?php echo $img['sizes']['large']; ?>" 
												alt="<?php echo $img['title'];?>" 
												width="<?php echo $img['width'];?>" 
												height="<?php echo $img['height'];?>" 
												data-srcset="
												<?php echo $img['sizes']['large'].' '.$img['sizes']['large-width'].'w'; ?>, 
												<?php echo $img['url'].' '.$img['width'].'w'; ?>,
												<?php echo $img['sizes']['medium'].' '.$img['sizes']['medium-width'].'w';?>, 
												<?php echo $img['sizes']['small'].' '.$img['sizes']['small-width'].'w'; ?>" >
											
											</div>
										<?php endif; ?>
										<div class="col-lg-6 col-md-12 text_area">
											<div class="textbox">
												<h3 class="title"><?php echo $title ?></h3>
												<p class="desription"><?php echo $description ?></p>
											</div>
										</div>
									</div>
								</div>
							</section>		

						<?php
					elseif( get_row_layout() == 'shop' ):
						$img = get_sub_field('img_right');
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$opensoon = get_sub_field('opensoon');
						$opensoon_text = get_sub_field('opensoon_text');?>

							<section class="sklep wow animate__fadeInUp" data-wow-duration="1s" id="shop">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
												<?php if( !empty( $opensoon ) ): ?>
											<div class="open_soon">
												<img class='lazy' 
												data-src="<?php echo $opensoon['sizes']['large']; ?>" 
												alt="<?php echo $opensoon['title'];?>" 
												width="<?php echo $opensoon['width'];?>" 
												height="<?php echo $opensoon['height'];?>" 
												data-srcset="
												<?php echo $opensoon['sizes']['large'].' '.$opensoon['sizes']['large-width'].'w'; ?>, 
												<?php echo $opensoon['url'].' '.$opensoon['width'].'w'; ?>,
												<?php echo $opensoon['sizes']['medium'].' '.$opensoon['sizes']['medium-width'].'w';?>, 
												<?php echo $opensoon['sizes']['small'].' '.$opensoon['sizes']['small-width'].'w'; ?>" >
											
												<p class="opensoon_text"><?php echo $opensoon_text ?></p>
											</div>
														<?php endif; ?>
													<div class="col-lg-6 col-md-12 text_area order-last">
														<div class="textbox">
															<h3 class="title"><?php echo $title ?></h3>
															<p class="desription"><?php echo $description ?></p>
														</div>
													</div>
														<?php if( !empty( $img ) ): ?>
													<div class="col-lg-6 col-md-12 img_right order-xl-last order-md-last">
														<img class='lazy' 
														data-src="<?php echo $img['sizes']['large']; ?>" 
														alt="<?php echo $img['title'];?>" 
														width="<?php echo $img['width'];?>" 
														height="<?php echo $img['height'];?>" 
														data-srcset="
														<?php echo $img['sizes']['large'].' '.$img['sizes']['large-width'].'w'; ?>, 
														<?php echo $img['url'].' '.$img['width'].'w'; ?>,
														<?php echo $img['sizes']['medium'].' '.$img['sizes']['medium-width'].'w';?>, 
														<?php echo $img['sizes']['small'].' '.$img['sizes']['small-width'].'w'; ?>" >
													</div>
														<?php endif; ?>
										</div>
									</div>
								</div>
							</section>		
	

						<?php
					elseif( get_row_layout() == 'aboutus' ):
						$background = get_sub_field('background');
						$description = add_nbsp(get_sub_field('description'));
						$button_text = get_sub_field('button_text');
						$url = get_sub_field('url');?>

						<section class="aboutus">
							<div class="container-fluid">
								<div class="container">
									<div class="row" >
										
								
									<div class="col-12">
										<div class="background">
											<img 
												class='lazy thumbnail' 
												data-src="<?php echo $background['sizes']['large']; ?>" 
												alt="<?php echo $background['title'];?>" 
												width="<?php echo $background['width'];?>" 
												height="<?php echo $background['height'];?>" 
												data-srcset="
												<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
												<?php echo $background['url'].' '.$background['width'].'w'; ?>,
												<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
												<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >
											<div class="textbox">
												<p class="desription"><?php echo $description ?></p>
												<a href="<?php echo get_home_url ();?><?php echo $url ?>">
													<span class="button_text"><?php echo $button_text ?></span>
												</a>
											</div>
										</div>
												
									</div>


									</div>
								</div>
							</div>
						</section>		

						<?php
						elseif( get_row_layout() == 'whatwedo' ):
						$background = get_sub_field('background');
						$title = add_nbsp(get_sub_field('title'));
						?>

						<section class="whatwedo">
							<div class="container-fluid">
								<div class="container">
									<div class="row" >
										<div class="col-12">
											<div class="background" style="background-image: url(<?php if( $background ): ?>
														<?php echo $background; ?>
														<?php endif; ?>)">
												<div class="textbox">
													
													<h2 class="title">
														<?php echo $title ?>
													</h2>
													<div class="row">

														<?php

															// Check rows exists.
															if( have_rows('box') ):

																// Loop through rows.
																while( have_rows('box') ) : the_row();

																	// Load sub field value.
																	$box_title = add_nbsp(get_sub_field('box_title'));
																	$box_image = get_sub_field('box_image');
																	$box_description = add_nbsp(get_sub_field('box_description'));?>

																	

																	<div class="col-lg-6 col-sm-12 whatwedo_box wow animate__fadeInUp" data-wow-duration="1s">
																		<p class="box_title">
																			<?php echo $box_title; ?>
																		</p>
																		<div class="box_image">
																			<?php if( $box_image ): ?>
																				<img src="<?php echo $box_image ?>" />
																			<?php endif; ?>
																		</div>	
																		<div class="box_description">
																			<?php echo $box_description; ?>
																		</div>
																	</div>	

																
																<?php
																// End loop.
																endwhile;

															// No value.
															else :
																// Do something...
															endif;
															?>
													</div>
												</div>
											</div>
													
										</div>


									</div>
								</div>
							</div>
						</section>		

						<?php
					elseif( get_row_layout() == 'circles' ):
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$button_text = get_sub_field('button_text');?>

						<section class="circles">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<h3 class="title wow animate__fadeInUp" data-wow-duration="3s">
												<?php echo $title ?>
											</h3>
										</div>
									</div>
										
											<div class="circle_area">

											<?php

												// Check rows exists.
												if( have_rows('circle_item') ):

													// Loop through rows.
													while( have_rows('circle_item') ) : the_row();

														// Load sub field value.
														$circle_content = get_sub_field('circle_content');
														?>

														<div class="col-4 circle_content wow animate__fadeInUp" data-wow-duration="3s">
															<p class="citcle_text">
																<?php echo $circle_content ?>
															</p>
														</div>
													
													<?php
													// End loop.
													endwhile;

												// No value.
												else :
													// Do something...
												endif;
												?>
										</div>
										
												
										
									
								</div>
							</div>
						</section>	


						<?php
					elseif( get_row_layout() == 'textarea' ):
						$textarea = add_nbsp(get_sub_field('textarea'));?>

						<section class="textarea">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="description wow animate__fadeIn" data-wow-duration="3s">
												<p>
													<?php echo $textarea ?>
												</p>
											</div>
										</div>
									</div>	
								</div>
							</div>
						</section>	

						<?php
					elseif( get_row_layout() == 'component_4' ):
						$dogimage = get_sub_field('dogimage');
						$title = add_nbsp(get_sub_field('title'));
						$opis = add_nbsp(get_sub_field('opis'));
						$opis_cd = add_nbsp(get_sub_field('opis_cd'));
						?>

						<section class="component_4">
								<div class="container-fluid">
									<div class="container">
										<div class="row" >
											
												<div class="dogimage"> 
													<?php if( !empty( $dogimage ) ): ?>
   													 <img 
														class='lazy' 
														data-src="<?php echo $dogimage['sizes']['large']; ?>" 
														alt="<?php echo $dogimage['title'];?>" 
														width="<?php echo $dogimage['width'];?>" 
														height="<?php echo $dogimage['height'];?>" 
														data-srcset="
														<?php echo $dogimage['sizes']['large'].' '.$dogimage['sizes']['large-width'].'w'; ?>, 
														<?php echo $dogimage['url'].' '.$dogimage['width'].'w'; ?>,
														<?php echo $dogimage['sizes']['medium'].' '.$dogimage['sizes']['medium-width'].'w';?>, 
														<?php echo $dogimage['sizes']['small'].' '.$dogimage['sizes']['small-width'].'w'; ?>" >
														<?php endif; ?>
												
													
														<div class="title wow animate__fadeInTopLeft" data-wow-duration="2s">
															<p><?php echo $title ?></p>
														</div>

														<div class="opis wow animate__fadeInRight" data-wow-duration="2s">
															
																<p><?php echo $opis ?></p>
																<p><?php echo $opis_cd ?></p>
															
														</div>
													
												</div>

										</div>
									</div>
								</div>
						</section>		

						<?php
					elseif( get_row_layout() == 'component_5' ):
						$background = get_sub_field('background');
						$opis = add_nbsp(get_sub_field('opis'));
						$opis_cd = add_nbsp(get_sub_field('opis_cd'));
						?>

						<section class="component_5">
								<div class="container-fluid">
									<div class="container">
										<div class="row" >
											
												<div class="component_5_img"> 
													<?php if( !empty( $background ) ): ?>
														<img 
														class='lazy' 
														data-src="<?php echo $background['sizes']['large']; ?>" 
														alt="<?php echo $background['title'];?>" 
														width="<?php echo $background['width'];?>" 
														height="<?php echo $background['height'];?>" 
														data-srcset="
														<?php echo $background['sizes']['large'].' '.$background['sizes']['large-width'].'w'; ?>, 
														<?php echo $background['url'].' '.$background['width'].'w'; ?>,
														<?php echo $background['sizes']['medium'].' '.$background['sizes']['medium-width'].'w';?>, 
														<?php echo $background['sizes']['small'].' '.$background['sizes']['small-width'].'w'; ?>" >
														<?php endif; ?>
													
													<div class="opis">
														<p class="wow animate__fadeInUp" data-wow-duration="1s"><?php echo $opis ?></p>
														<p class="wow animate__fadeInUp" data-wow-duration="1s"><?php echo $opis_cd ?></p>
													</div>
												</div>

										</div>
									</div>
								</div>
						</section>		

						<?php
					elseif( get_row_layout() == 'textarea_2p' ):
						$textarea = add_nbsp(get_sub_field('textarea'));
						$textarea_cd = add_nbsp(get_sub_field('textarea_cd'));?>

						<section class="textarea_2p">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="description_box">
												<div class="description wow animate__fadeIn" data-wow-duration="3s">
													<p>
														<?php echo $textarea ?>
													</p>
													<p>
														<?php echo $textarea_cd ?>
													</p>
												</div>
											</div>
											</div>
										</div>
									</div>	
								</div>
							</div>
						</section>	


						<?php
					elseif( get_row_layout() == 'big_img' ):
					$big_image = get_sub_field('big_image');
					?>

						<section class="big_img wow animate__fadeInUp" data-wow-duration="2s">
   													 <img 
														class='lazy thumbnail' 
														data-src="<?php echo $big_image['sizes']['large']; ?>" 
														alt="<?php echo $big_image['title'];?>" 
														width="<?php echo $big_image['width'];?>" 
														height="<?php echo $big_image['height'];?>" 
														data-srcset="
														<?php echo $big_image['sizes']['large'].' '.$big_image['sizes']['large-width'].'w'; ?>, 
														<?php echo $big_image['url'].' '.$big_image['width'].'w'; ?>,
														<?php echo $big_image['sizes']['medium'].' '.$big_image['sizes']['medium-width'].'w';?>, 
														<?php echo $big_image['sizes']['small'].' '.$big_image['sizes']['small-width'].'w'; ?>" >
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										

									
									</div>
								</div>
							</div>
						</section>


						<?php
					elseif( get_row_layout() == 'doberman_lines' ):
						$main_title = get_sub_field('main_title');
						$line1_img = get_sub_field('line1_img');
						$line2_img = get_sub_field('line2_img');
						$line1_title = get_sub_field('line1_title');
						$line2_title = get_sub_field('line2_title');
						$line1_description = add_nbsp(get_sub_field('line1_description'));
						$line2_description = add_nbsp(get_sub_field('line2_description'));
						?>

						<section class="doberman_lines">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="main_title_box">
												<div class="main_title">
													<p><?php echo $main_title?></p>
												</div>
											</div>
										</div>	
										

										
										<div class="row img_box">
												
												<div class="col-lg-6 col-sm-12">
												<div class="line_box">
													<div class="wow animate__fadeInLeft" data-wow-duration="3s">
														<?php if( !empty( $line1_img ) ): ?>
															<img 
															class='lazy' 
															data-src="<?php echo $line1_img['sizes']['large']; ?>" 
															alt="<?php echo $line1_img['title'];?>" 
															width="<?php echo $line1_img['width'];?>" 
															height="<?php echo $line1_img['height'];?>" 
															data-srcset="
															<?php echo $line1_img['sizes']['large'].' '.$line1_img['sizes']['large-width'].'w'; ?>, 
															<?php echo $line1_img['url'].' '.$line1_img['width'].'w'; ?>,
															<?php echo $line1_img['sizes']['medium'].' '.$line1_img['sizes']['medium-width'].'w';?>, 
															<?php echo $line1_img['sizes']['small'].' '.$line1_img['sizes']['small-width'].'w'; ?>" >
															<?php endif; ?>
													
												
													<p class="line_title"><?php echo $line1_title?></p>
													</div>
												</div>
													<p class="line_description"><?php echo $line1_description?></p>
														</div>
											
												<div class="col-lg-6 col-sm-12">
													<div class="line_box">
													<div class="wow animate__fadeInRight" data-wow-duration="3s">
														<?php if( !empty( $line2_img ) ): ?>
															<img 
															class='lazy' 
															data-src="<?php echo $line2_img['sizes']['large']; ?>" 
															alt="<?php echo $line2_img['title'];?>" 
															width="<?php echo $line2_img['width'];?>" 
															height="<?php echo $line2_img['height'];?>" 
															data-srcset="
															<?php echo $line2_img['sizes']['large'].' '.$line2_img['sizes']['large-width'].'w'; ?>, 
															<?php echo $line2_img['url'].' '.$line2_img['width'].'w'; ?>,
															<?php echo $line2_img['sizes']['medium'].' '.$line2_img['sizes']['medium-width'].'w';?>, 
															<?php echo $line2_img['sizes']['small'].' '.$line2_img['sizes']['small-width'].'w'; ?>" >
													
													<?php endif; ?>

														<p class="line_title"><?php echo $line2_title?></p>
													</div>
												</div>
														<p class="line_description"><?php echo $line2_description?></p>
														</div>
											
										
										</div>
									</div>	
								</div>
							</div>
						</section>	


						<?php
					elseif( get_row_layout() == 'component_6' ):
						$background = get_sub_field('background');
						$title = add_nbsp(get_sub_field('title'));
						$race_img_1 = get_sub_field('race_img_1');
						$descr_race_1 = add_nbsp(get_sub_field('descr_race_1'));
						$race_img_2 = get_sub_field('race_img_2');
						$descr_race_2 = add_nbsp(get_sub_field('descr_race_2'));
						$race_img_3 = get_sub_field('race_img_3');
						$descr_race_3 = add_nbsp(get_sub_field('descr_race_3'));
						?>

						<section class="component_6">
								<div class="container-fluid">
									<div class="container">
										<div class="row" >
											
												<div class="component_6_img" style="background-image: url(<?php if( $background ): ?>
														<?php echo $background; ?>
														<?php endif; ?>)"> 

													<div class="yellow_background"></div>
													
													
													<p class="title"><?php echo $title ?></p>
												

													
													<div class="race_1">
															<?php if( !empty( $race_img_1 ) ): ?>
															
																<img 
																class='lazy race_img_1' 
																data-src="<?php echo $race_img_1['sizes']['large']; ?>" 
																alt="<?php echo $race_img_1['title'];?>" 
																width="<?php echo $race_img_1['width'];?>" 
																height="<?php echo $race_img_1['height'];?>" 
																data-srcset="
																<?php echo $race_img_1['sizes']['large'].' '.$race_img_1['sizes']['large-width'].'w'; ?>, 
																<?php echo $race_img_1['url'].' '.$race_img_1['width'].'w'; ?>,
																<?php echo $race_img_1['sizes']['medium'].' '.$race_img_1['sizes']['medium-width'].'w';?>, 
																<?php echo $race_img_1['sizes']['small'].' '.$race_img_1['sizes']['small-width'].'w'; ?>" >
															
															<?php endif; ?>	
															<p class="descr_race_1 wow animate__fadeInRight" data-wow-duration="2s">
																<?php echo $descr_race_1 ?>
															</p>
													</div>
													

													<div class="race_2">
													<?php if( !empty( $race_img_2 ) ): ?>
														<img 
														class='lazy race_img_2' 
														data-src="<?php echo $race_img_2['sizes']['large']; ?>" 
														alt="<?php echo $race_img_2['title'];?>" 
														width="<?php echo $race_img_2['width'];?>" 
														height="<?php echo $race_img_2['height'];?>" 
														data-srcset="
														<?php echo $race_img_2['sizes']['large'].' '.$race_img_2['sizes']['large-width'].'w'; ?>, 
														<?php echo $race_img_2['url'].' '.$race_img_2['width'].'w'; ?>,
														<?php echo $race_img_2['sizes']['medium'].' '.$race_img_2['sizes']['medium-width'].'w';?>, 
														<?php echo $race_img_2['sizes']['small'].' '.$race_img_2['sizes']['small-width'].'w'; ?>" >
														<?php endif; ?>
														<p class="descr_race_2 wow animate__fadeInLeft" data-wow-duration="2s"><?php echo $descr_race_2 ?></p>
													</div>

													<div class="race_3">
													<?php if( !empty( $race_img_3 ) ): ?>
														<img 
														class='lazy race_img_3' 
														data-src="<?php echo $race_img_3['sizes']['large']; ?>" 
														alt="<?php echo $race_img_3['title'];?>" 
														width="<?php echo $race_img_3['width'];?>" 
														height="<?php echo $race_img_3['height'];?>" 
														data-srcset="
														<?php echo $race_img_3['sizes']['large'].' '.$race_img_3['sizes']['large-width'].'w'; ?>, 
														<?php echo $race_img_3['url'].' '.$race_img_3['width'].'w'; ?>,
														<?php echo $race_img_3['sizes']['medium'].' '.$race_img_3['sizes']['medium-width'].'w';?>, 
														<?php echo $race_img_3['sizes']['small'].' '.$race_img_3['sizes']['small-width'].'w'; ?>" >
														<?php endif; ?>
														<p class="descr_race_3 wow animate__fadeInRight" data-wow-duration="2s"><?php echo $descr_race_3 ?></p>
													</div>
												</div>

										</div>
									</div>
								</div>
						</section>

						<?php
						elseif( get_row_layout() == 'under_construction' ):
						$under_construction_text = get_sub_field('under_construction_text');
						$opensoon = get_sub_field('opensoon_img');
						$background = get_sub_field('background');
						$text = get_sub_field('text');
						?>

						<section class="under_construction">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											<div class="opensoon">
												<?php if( !empty( $opensoon ) ): ?>
												<div class="opensoon_img">
													<img 
														class='lazy' 
														data-src="<?php echo $opensoon['sizes']['large']; ?>" 
														alt="<?php echo $opensoon['title'];?>" 
														width="<?php echo $opensoon['width'];?>" 
														height="<?php echo $opensoon['height'];?>" 
														data-srcset="
														<?php echo $opensoon['sizes']['large'].' '.$opensoon['sizes']['large-width'].'w'; ?>, 
														<?php echo $opensoon['url'].' '.$opensoon['width'].'w'; ?>,
														<?php echo $opensoon['sizes']['medium'].' '.$opensoon['sizes']['medium-width'].'w';?>, 
														<?php echo $opensoon['sizes']['small'].' '.$opensoon['sizes']['small-width'].'w'; ?>" >
														<p class="opensoon_text"><?php echo $under_construction_text ?></p>
												</div>
												
													<?php endif; ?>
													<p class="text"><?php echo $text ?></p>
												</div>
											</div>
										</div>
									</div>	
								</div>
							</div>
						</section>	
<?php
					elseif( get_row_layout() == 'transport' ):
						$transport_textarea = add_nbsp(get_sub_field('transport_textarea'));
						?>

						<section class="transport">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="col-12">
											
												
													<p>
														<?php echo $transport_textarea ?>
													</p>

												
											
											</div>
										</div>
									</div>	
								</div>
							</div>
						</section>	



						
					<?php
					endif;
								
				endwhile;
				else :
				endif;
			?>
		</div>
	<?php wp_reset_postdata(); ?>

				<?php
			// Check value exists.
			if( have_rows('flexible_content') ):

				// Loop through rows.
				while ( have_rows('flexible_content') ) : the_row();
				
							if( get_row_layout() == 'content' ):

								if( have_rows('section') ):
									while ( have_rows('section') ) : the_row();
								
									if( get_row_layout() == 'columns_1' ):
										$col_1 = get_sub_field('col_1');
										echo '<div class="row">';
										echo '<div class="col-12">'.$col_1.'</div>';
										echo '</div>';
									
									elseif( get_row_layout() == 'columns_2' ):
										$col_1 = get_sub_field('col_1');
										$col_2 = get_sub_field('col_2');
							
										echo '<div class="row">';
											echo '<div class="col-lg-6 col-sm-12 col-md-12">'.$col_1.'</div>';
											echo '<div class="col-lg-6 col-sm-12 col-md-12">'.$col_2.'</div>';
										echo '</div>';
									elseif( get_row_layout() == 'columns_3' ):
										$col_1 = get_sub_field('col_1');
										$col_2 = get_sub_field('col_2');
										$col_3 = get_sub_field('col_3');
									
										echo '<div class="row">';
										echo '<div class="col-4">'.$col_1.'</div>';
										echo '<div class="col-4">'.$col_2.'</div>';
										echo '<div class="col-4">'.$col_3.'</div>';
									echo '</div>';
									endif;	
								
									endwhile;
								endif;
							endif;
						
							
				// End loop.
				endwhile;

			// No value.
			else :
				// Do something...
			endif;

?>

<?php wp_reset_postdata(); ?>