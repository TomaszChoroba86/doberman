<?php
	// WP_Query arguments
	$args = array(
		'post_type'=>"szczenieta",
		'post_status'=>"publish",
		'meta_key' => 'puppies_status',
		'meta_value' => 'true'
	);

	// The Query
		$pups = new WP_Query( $args );
		$count = $pups->found_posts;
			if($count==3) {
				$jscount = 3;
				$extraclass = 'fourofitems';
					}elseif($count>=4){
						$jscount = 4;
						$extraclass = 'fourofitems';
					}else{
							$jscount = $count;
					}if ($count>0) : ?>
						<section class="section-title">
							<div class="container-fluid">
								<div class="container">
									<div class="row">
										<div class="puppies-title-box">
											<h1 class="col-12 puppies-title"><?php echo $template_title?></h1>
										</div>
									</div>
								</div>
							</div>
						</section>

	<div class="container-fluid" id="puppies">
		<div class="container">
			<div class="row puppies-photos">
				<div class="owl-carousel owl-theme">
					<?php
// The Loop
									if ( $pups->have_posts() ) {
										while ( $pups->have_posts() ) {
											$pups->the_post();

											$permalink = get_permalink ( $post->ID );
											$title = get_the_title ( $post->ID );
											$content = get_field('puppies_descr');
											$button_descr = get_field('puppies_button_descr');
											$image = get_the_post_thumbnail_url( $post->ID );
											$puppies_img_horizontal = get_field('puppies_img_horizontal');
											?>

											<div class="link-box">
											<div class="overlay"></div>
												<div class="puppies-img">
														<div class="img-box">

															<?php
															if( !empty( $puppies_img_horizontal ) ): ?>
															<div class="horizontal-img <?php echo $extraclass?>">
																<img data-src="<?php echo esc_url($puppies_img_horizontal['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="owl-lazy"/>
																</div>
															<?php endif; ?>
														
															<div class="regular-img">	
																<img data-src="<?php echo ( $image );?>" class="owl-lazy">
															</div>
														</div>
													
														<div class="pup-box">
														<a href="<?php echo esc_url( $permalink ); ?>"><h3 class="pup-name"><?php echo ( $title );?></h3></a>
															<p class="puppies_descr"><?php echo ( $content );?></p>
															<span class="puppies_button"><?php echo ( $button_descr );?></span>
														</div>
												</div>
															
											
											</div>
											<?php

												}
											} else {
												// no posts found
											}
											wp_reset_postdata();
											?>


			</div>
		</div>
	</div>
</div>

					<?php

							echo '<script>';
							echo 'window.jscount = '.$jscount;
							echo '</script>';

					?>
<?php
endif;
?>
