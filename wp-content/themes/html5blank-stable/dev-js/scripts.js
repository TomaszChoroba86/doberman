function action(){

var myLazyLoad;
var isAnimatingScroll = false;

function beginLazyLoading(){
  if(myLazyLoad!=undefined){
    if(myLazyLoad.loadingCount!=undefined){
      myLazyLoad.destroy();
    }
  }
  myLazyLoad = new LazyLoad({
      elements_selector: ".lazy",
      threshold: 0
  });
}
beginLazyLoading();

$('#preloader').fadeOut();

new WOW().init();

function c_scrollTo(link){
  if(myLazyLoad!=undefined){
    if(myLazyLoad.loadingCount!=undefined){
      myLazyLoad.destroy();
    }
  }
  
  var scrollTop = $('html').scrollTop();
  var targetY = $(link).offset().top;
  var speed = Math.round((targetY-scrollTop)/1000)*500;
  speed = Math.abs(speed);
  isAnimatingScroll = true;
  $('html,body').animate({scrollTop: $(link).offset().top}, speed, function(){
        isAnimatingScroll = false;
        beginLazyLoading();
  });
}

function scrollToNoAnimation(link){
  // if($(link)[0]!=undefined){
    if(myLazyLoad!=undefined){
      if(myLazyLoad.loadingCount!=undefined){
    myLazyLoad.destroy();
      }
    }
    
    isAnimatingScroll = true;
    $('html,body').animate({scrollTop: $(link).offset().top}, 0, function(){
          isAnimatingScroll = false;
          beginLazyLoading();
    });
  // }
}

$('header .disabled').click(function(e){
	e.preventDefault();
})

var expanded = false;
$('.menu-expand-btn').click(function(){
	if(!expanded){
		expanded = true;
		$('.menu-expandable').slideDown();
		$('header').addClass('expanded');
	}else{
		expanded = false;
		$('.menu-expandable').hide();
		$('header').removeClass('expanded');
	}
})


$('.menu-expand-btn').on('click',changeState);
function changeState(){
  if($('.menu-expand-btn').hasClass('is-active')){
    // $('.menu-expand-btn').removeClass('is-active');
    $('.main-menu').removeClass('expanded');
    $('header').removeClass('expanded');
  }else{
    // $('.menu-expand-btn').addClass('is-active');
    $('.main-menu').addClass('expanded');
    $('header').addClass('expanded');
  }
}

$('.main-menu a').on('click',a_changeState);
function a_changeState(){
  if($('.main-menu').hasClass('expanded')){
    $('.main-menu').removeClass('expanded');
    $('.menu-expand-btn').removeClass('is-active');
  }else{
    $('.main-menu').addClass('expanded');
    $('.menu-expand-btn').addClass('is-active');
  }
}

    $('.menu-item-has-children').on('click',arrowExpand);
    function arrowExpand(){
      if($('.sub-menu').hasClass('showmore')){
        $('.sub-menu').removeClass('showmore');
      }else{
        $('.sub-menu').addClass('showmore');
      }
    }

    
window.onscroll = function() {scrollFunction()};
var sticky = 70;
var lastScrollTop = 0;

function scrollFunction() {
  if($(".main-menu").hasClass('expanded')==false){
    if (window.pageYOffset >= sticky) {
      $('header#main').addClass("sticky");
    } else {
      $('header#main').removeClass("sticky");
    }
  }

  var st = $(this).scrollTop();

  if($('.main-menu').hasClass('expanded')==false){
    if (st > lastScrollTop){
       // downscroll code
       $('header#main').removeClass("scroll-up");
    } else {
      // upscroll code
      $('header#main').addClass("scroll-up");
    }
    lastScrollTop = st;
  }
}



  var hamburger = document.querySelector(".hamburger");
  hamburger.addEventListener("click", function() {
    hamburger.classList.toggle("is-active");
  });

  function add_nbsp($title, $id = null){
    return preg_replace('/ ([A-Za-z+-D]{1}) /', " $1&nbsp;", $title);
  }
 
  
  $('.link-box').on('click',function(e){
    if($(this).find('.sub-menu-option')[0]==undefined) {
      e.stopPropagation();
      e.preventDefault();
      var link = $(this).find('a').attr('href');
      if(link.indexOf("#")!=-1){
        var anchor = link.substring(link.indexOf("#")+1);
        if($('#'+anchor)[0]!=undefined){
          c_scrollTo('#'+anchor);
        }else{
          window.open(link,'_self');
          $('#preloader').fadeIn();
        }
      }else{
        window.open(link,'_self');
        $('#preloader').fadeIn();
      }
    }
  })

  $('a').on('click',function(e){
    if($(this).find('img')[0]==undefined) {
    e.preventDefault();
        var link = $(this).attr('href');
        if(link.indexOf("#")!=-1){
          var anchor = link.substring(link.indexOf("#")+1);
          if($('#'+anchor)[0]!=undefined){
            c_scrollTo('#'+anchor);
          }else{
            window.open(link,'_self');
            $('#preloader').fadeIn();
          }
        }else if ($(this).hasClass('menu-item-has-children')==true) {
          window.open(link,'_self');
          $('#preloader').fadeIn();
        }else{
          window.open(link,'_self');
          $('#preloader').fadeIn();
         }
        }
  })


   
  if(window.location.hash) {
    setTimeout(blober,500);
    function blober(){
    scrollToNoAnimation(window.location.hash);
    }
  }


  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "rgba(0, 0, 0, 0.7)",
        "text": "#FFFFFF"
      },
      "button": {
        "background": "#af945e",
        "text": "#FFFFFF"
      }
    },
    "content": {
      "message": "Strona korzysta z plików cookie w celu realizacji usług zgodnie z Polityką prywatności i ochrona danych osobowych. Możesz określić warunki przechowywania lub dostępu do cookie w Twojej przeglądarce lub konfiguracji usługi.",
      "dismiss": "Rozumiem",
      "link": "Dowiedz się więcej",
      "href": "/polityka-prywatnosci"
    }
  })

}

  function checkForJquery(){
    if(typeof $ == 'function') {
      action();
    }else{
      setTimeout(checkForJquery,500);
    }
  }
  setTimeout(checkForJquery,500);

  
