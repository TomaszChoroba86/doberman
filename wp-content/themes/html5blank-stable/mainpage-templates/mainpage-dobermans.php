<?php
									// WP_Query arguments
									$args = array(
										'post_type'=>"dobermans",
										'post_status'=>"publish",
										'meta_key' => 'dobermans_status',
										'meta_value' => 'true'
									);

									// The Query
									$dobermans = new WP_Query( $args );
									
									$count = $dobermans->found_posts;
									if($count==3) {
										$jscount = 3;
										$extraclass = 'fourofitems';
									}elseif($count>=4){
										$jscount = 4;
										$extraclass = 'fourofitems';
									}else{
										$jscount = $count;
									}
									if ($count>0) : ?>
									<!-- <section class="section-title">
										<div class="container-fluid">
											<div class="container">
												<div class="row">
													<div class="dobermans-title-box">
														<h1 class="col-12 dobermans-title"><?php echo $template_title?></h1>
													</div>
												</div>
											</div>
										</div>
									</section> -->

	<div class="container-fluid" id="dobermans">
		<div class="container">
			<div class="row dobermans-photos">
				<div class="owl-carousel owl-theme">
					<?php

									// The Loop
									if ( $dobermans->have_posts() ) {
										while ( $dobermans->have_posts() ) {
											$dobermans->the_post();

											$permalink = get_permalink ( $post->ID );
											$title = get_the_title ( $post->ID );
											$content = get_field('dobermans_descr');
											$button_descr = get_field('dobermans_button_descr');
											$image = get_the_post_thumbnail_url( $post->ID );
											$dobermans_img_horizontal = get_field('dobermans_img_horizontal');
											?>



										<div class="link-box">
											<div class="overlay"></div>
												<div class="dobermans-img">
													<div class="img-box">

															<?php
															if( !empty( $dobermans_img_horizontal ) ): ?>
															<div class="horizontal-img <?php echo $extraclass?>">
																<img data-src="<?php echo esc_url($dobermans_img_horizontal['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="owl-lazy"/>
																</div>
															<?php endif; ?>


														<div class="regular-img">
															<img data-src="<?php echo ( $image );?>" class="owl-lazy">
														</div>
													</div>
														<div class="dobermans-box">
														<a href="<?php echo esc_url( $permalink ); ?>"><h3 class="dobermans-name"><?php echo ( $title );?></h3></a>
															<p class="dobermans_descr"><?php echo ( $content );?></p>
															<span class="dobermans_button"><?php echo ( $button_descr );?></span>
														</div>
												</div>
											</div>

											<?php

												}
											} else {
												// no posts found
											}
											wp_reset_postdata();
											?>


							</div>
						</div>
	</div>
</div>




					<?php

							echo '<script>';
							echo 'window.jscount = '.$jscount;
							echo '</script>';

					?>
<?php
endif;
?>
